//
//  BaseTableViewCell.m
//  projectSnowball
//
//  Created by Donald Fung on 1/23/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "Constants.h"


@implementation BaseTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (NSString *)stringFromNumberWithDecimalStyle:(NSNumber *)number
{
    NSNumberFormatter *numberFormat = [[NSNumberFormatter alloc]init];
    [numberFormat setNumberStyle:NSNumberFormatterDecimalStyle];
    numberFormat.maximumFractionDigits = 0;
    NSString *stringFromNumber = [numberFormat stringFromNumber:number];
    return stringFromNumber;
}

- (NSString *)formatNumberCurrencyStyle:(NSNumber *)number
{
    float num = [number floatValue]/1000000;
    NSNumber *formattedNum = [NSNumber numberWithFloat:num];
    //  Settings for labels that require curreny and decimal formatting
    NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
    [currencyStyle setNumberStyle: NSNumberFormatterCurrencyStyle];
    currencyStyle.maximumFractionDigits = 0;
    NSString *figure = [currencyStyle stringFromNumber:formattedNum];
    return figure;
}

- (NSString *)formatNumberCurrencyStyleForPricesAndRatios:(NSNumber *)number
{
    //  Settings for labels that require curreny and decimal formatting
    NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
    [currencyStyle setNumberStyle: NSNumberFormatterCurrencyStyle];
    currencyStyle.maximumFractionDigits = 2;
    NSString *figure = [currencyStyle stringFromNumber:number];
    return figure;

}

- (NSString *)formatNumberNoStyle:(NSNumber *)number
{
    //  Settings for ratios and other non currency strings
    NSNumberFormatter *normalStyle = [[NSNumberFormatter alloc] init];
    [normalStyle setNumberStyle: NSNumberFormatterNoStyle];
    normalStyle.maximumFractionDigits = 2;
    NSString *figure = [normalStyle stringFromNumber:number];
    return figure;
}


#pragma mark Set Font
- (UIFont *)setFontToHelveticaWithSizeTwelve:(UILabel *)label
{
    label.font = [UIFont fontWithName:helvetica_neue size:size_twelve];
    return label.font;
}

- (UIFont *)setFontToHelveticaWithSizeFourteen:(UILabel *)label
{
    label.font = [UIFont fontWithName:helvetica_neue size:size_fourteen];
    return label.font;
}

#pragma mark Company Data
- (Company *)getCompanyDataForCell
{
    self.companyManager = [CompanyManager sharedCompany];
    Company *company = self.companyManager.company;
    return  company;
}

#pragma mark Labels
- (UILabel *)customizeLabelsForTenYearTableView:(UITableViewCell *)cell
{
    //  Standard settings for labels.  Override in some other method
    cell.textLabel.font = [UIFont fontWithName:helvetica_neue_medium size:size_fourteen];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    return cell.textLabel;
}
@end
