//
//  BaseViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 1/15/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "BaseViewController.h"

@implementation BaseViewController

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUpTopNav];
    [self getScreenSize];
    self.baseViewDelegate = self;
}

#pragma mark - Top Nav Bar

- (void)setNavBarTitle:(NSString *)title
{
    self.navigationItem.title = title;
}

- (void)setUpTopNav
{
    //  Set title's font
    [self setTitleSizeAndFontAndColor:[UIColor whiteColor]];
    
    //  Set bar tint color
    [self setBarTintColor:[UIColor darkBlue]];
    
    //  Set color of bar button items
    [self setTintColor:[UIColor whiteColor]];
    
    self.navigationController.navigationBar.translucent = NO;
}

//  Helper methods


- (void)setTitleColor:(UIColor *)color
{
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: color};
}

- (void)setBarTintColor:(UIColor *)color
{
    self.navigationController.navigationBar.barTintColor = color;
}

- (void)setTintColor:(UIColor *)color
{
    [self.navigationController.navigationBar setTintColor:color];
}

- (void)setTitleSizeAndFontAndColor:(UIColor *)color
{
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:color,
       NSFontAttributeName:[UIFont fontWithName:helvetica_neue_bold size:size_eighteen]}];
}

- (void)customizeLeftBarButton
{
    self.navigationItem.backBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:nil
                                     style:UIBarButtonItemStylePlain
                                    target:nil
                                    action:nil];
}

//  Screen size

- (CGRect)getScreenSize
{
    CGRect screenRectangle = [[UIScreen mainScreen] bounds];
    screenWidth = screenRectangle.size.width;
    screenHeight = screenRectangle.size.height;
    return screenRectangle;
}

#pragma mark Add tableview as subview
- (UITableViewController*)tableViewAsSubview:(UITableViewController *)tvc withIdentifier:(NSString *)identifier andTableViewHolder:(UIView *)tableviewHolder andFrame:(CGRect)tableviewFrame
{
    tvc = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
    tvc.view.frame = tableviewFrame;
    [self addChildViewController:tvc];
    [tableviewHolder addSubview:tvc.view];
    [tvc didMoveToParentViewController:self];
    return tvc;
}

#pragma mark - Number formatter

- (NSString *)formatNumberCurrencyStyle:(NSNumber *)number
{
    //    float num = [number floatValue]/1000000;
    //    NSNumber *formattedNum = [NSNumber numberWithFloat:num];
    //  Settings for labels that require curreny and decimal formatting
    self.currencyFormat = [[NSNumberFormatter alloc] init];
    [self.currencyFormat setNumberStyle: NSNumberFormatterCurrencyStyle];
    // self.currencyFormat.maximumFractionDigits = 2;
    NSString *figure = [self.currencyFormat stringFromNumber:number];
    return figure;
}

- (NSString *)formatNumberNoStyle:(NSNumber *)number
{
    //  Settings for ratios and other non currency strings
    self.decimalFormat = [[NSNumberFormatter alloc] init];
    [self.decimalFormat  setNumberStyle: NSNumberFormatterNoStyle];
    self.decimalFormat .maximumFractionDigits = 2;
    NSString *figure = [self.decimalFormat stringFromNumber:number];
    return figure;
}

#pragma mark Company Data
- (Company *)getCompanyDataForViewController
{
    self.companyManager = [CompanyManager sharedCompany];
    Company *company = self.companyManager.company;
    return  company;
}

#pragma mark Summary Table UI
- (UILabel *)customizeLabelForSummaryView:(UILabel *)label
{
    //  Standard settings for labels.  Override in some other method
    label.font = [UIFont fontWithName:helvetica_neue size:size_fourteen];
    label.textColor = [UIColor whiteColor];
    return label;
}

#pragma mark Date formats

- (NSString *)getYear:(NSString *)date
{
    NSArray *setOfStrings = [date componentsSeparatedByString:@"-"];
    NSString *year = [setOfStrings objectAtIndex:0];
    return year;
}

#pragma mark Labels

- (UIFont *)setFontToHelveticaWithSizeFourteen
{
    UILabel *label;
    label.font = [UIFont fontWithName:helvetica_neue size:size_fourteen];
    return label.font;
}

- (UIFont *)setFontToHelveticaWithSizeSixteen
{
    UILabel *label;
    label.font = [UIFont fontWithName:helvetica_neue size:size_sixteen];
    return label.font;
}

@end
