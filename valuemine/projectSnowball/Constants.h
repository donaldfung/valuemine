//
//  Constants.h
//  projectSnowball
//
//  Created by Donald Fung on 11/11/15.
//  Copyright © 2015 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <Foundation/Foundation.h>

static const int number_of_requests = 20;
@interface Constants : NSObject

@property NSMutableArray *urlArrray;
- (NSMutableArray *)arrayOfQuandlStrings;

//  Quandl API
#define QuandlAPIKey                @"BKb5Wt5ztRzUkqScMyeJ"
#define QuandlPassword @"DawgScholes11"

//  Income Statement Data
#define AnnualRevenues              @"https://www.quandl.com/api/v3/datasets/SF1/%@_REVENUE_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define NetIncome                   @"https://www.quandl.com/api/v3/datasets/SF1/%@_NETINC_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define CostOfRevenue               @"https://www.quandl.com/api/v3/datasets/SF1/%@_COR_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define DividendsPerShare           @"https://www.quandl.com/api/v3/datasets/SF1/%@_DPS_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
//  Balance Sheet Data
#define RetainedEarnings            @"https://www.quandl.com/api/v3/datasets/SF1/%@_RETEARN_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define RetainedEarningsQuarterly   @"https://www.quandl.com/api/v3/datasets/SF1/%@_RETEARN_ARQ.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define Cash                        @"https://www.quandl.com/api/v3/datasets/SF1/%@_CASHNEQ_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define CashQuarterly               @"https://www.quandl.com/api/v3/datasets/SF1/%@_CASHNEQ_ARQ.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define SharesOutstanding           @"https://www.quandl.com/api/v3/datasets/SF1/%@_SHARESBAS.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define WeightedSharesDiluted       @"https://www.quandl.com/api/v3/datasets/SF1/%@_SHARESWA_ARQ.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define GoodWillAndIntangibleAssets @"https://www.quandl.com/api/v3/datasets/SF1/%@_INTANGIBLES_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define IntangiblesQuarterly        @"https://www.quandl.com/api/v3/datasets/SF1/%@_INTANGIBLES_ARQ.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define CurrentAssets               @"https://www.quandl.com/api/v3/datasets/SF1/%@_ASSETSC_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define CurrentAssetsQuartely       @"https://www.quandl.com/api/v3/datasets/SF1/%@_ASSETSC_ARQ.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define TotalAssets                 @"https://www.quandl.com/api/v3/datasets/SF1/%@_ASSETS_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define TotalAssetsQuareterly       @"https://www.quandl.com/api/v3/datasets/SF1/%@_ASSETSC_ARQ.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define TotalDebt                   @"https://www.quandl.com/api/v3/datasets/SF1/%@_DEBT_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define TotalDebtQuarterly          @"https://www.quandl.com/api/v3/datasets/SF1/%@_DEBT_ARQ.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define LongTermLiabilities         @"https://www.quandl.com/api/v3/datasets/SF1/%@_LIABILITIESNC_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define LongTermLiabilitiesARQ      @"https://www.quandl.com/api/v3/datasets/SF1/%@_LIABILITIESNC_ARQ.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define TotalLiabilities            @"https://www.quandl.com/api/v3/datasets/SF1/%@_LIABILITIES_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define TotalLiabilitiesARQ         @"https://www.quandl.com/api/v3/datasets/SF1/%@_LIABILITIES_ARQ.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define EarningsPerShare            @"https://www.quandl.com/api/v3/datasets/SF1/%@_EPS_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define EarningsPerShareARQDiluted  @"https://www.quandl.com/api/v3/datasets/SF1/%@_EPSDIL_ARQ.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define ShareholdersEquity          @"https://www.quandl.com/api/v3/datasets/SF1/%@_EQUITY_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define ShareholdersEquityARQ       @"https://www.quandl.com/api/v3/datasets/SF1/%@_EQUITY_ARQ.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define Dividends                   @"https://www.quandl.com/api/v3/datasets/SF1/%@_NCFDIV_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define INVENTORY                   @"https://www.quandl.com/api/v3/datasets/SF1/%@_INVENTORY_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define CURRENTLIABILITIES          @"https://www.quandl.com/api/v3/datasets/SF1/%@_LIABILITIESC_ARY.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define HistoricalPrices            @"https://www.quandl.com/api/v3/datasets/SF1/%@_PRICE.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"
#define MarketCap @"https://www.quandl.com/api/v3/datasets/SF1/%@_MARKETCAP.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"

//  Wiki EOD Stock Prices
#define CurrentStockPrice           @"https://www.quandl.com/api/v3/datasets/WIKI/%@.json?auth_token=BKb5Wt5ztRzUkqScMyeJ"

//  Admin Emails
#define donald_email @"donaldtfung@gmail.com"
#define blayne_email @"blaynechong@gmail.com"

//  Text Files
#define nyse @"nyse.txt"
#define nasdaq @"nasdaq.txt"
#define quandlList @"WIKI_tickers.csv"

//  Time - hours
#define testTime @"02:35:00"
#define midnight @"00:00:00"
#define one_am @"01:00:00"
#define six_pm @"18:00:00"

//  Screen Titles
#define watclist_title @"Watchlist"

// Company Ratio keys (keys for dictionary to be sent to Parse)
#define pe_ratio @"peRatio"
#define book_value @"bookValue"
#define book_value_per_share @"bookValuePerShare"
#define price_to_book_value @"priceToBookValue"
#define debt_to_capital_ratio @"debtToCapitalRatio"
#define current_ratio @"currentRatio"
#define quick_ratio @"quickRatio"
#define dividends_per_share @"dividendsPerShare"
#define net_current_asset_value @"ncav"
#define price_earnings_growth @"peg"

//  Row Heights
#define SearchCellHeight 52.0f
#define Watchlist_Cell_Height 46.0f
#define CompanyDetail_Cell_Height 38.0f

//  Row Labels
#define cash_label @"Cash & Cash Equivalents"
#define currentAssets_label @"Current Assets"
#define dividends_label @"Dividends"
#define goodwill_label @"Goodwill & Intangible Assets"
#define netIncome_label @"Net Income"
#define longtermLiab_label @"Other Long Term Liabilities"
#define retainedEarnings_label @"Retained Earnings"
#define revenue_label @"Revenue"
#define shareholderEquity_label @"Shareholder Equity"
#define sharesoutstanding_label @"Shares Outstanding"
#define totalAssets_label @"Total Assets"
#define totalDebt_label @"Total Debt"
#define totalLiab_label @"Total Liabilites"

//  Company Data - Keys
#define dividendsPerShare_key @"DPS"
#define longtermliabilities_key @"LIABILITIESNC"
#define totalLiabilities_key @"LIABILITIES"
#define debt_key @"DEBT"
#define currentAssets_key @"ASSETSC"
#define currentLiab_key @"LIABILITIESC"
#define intangibles_key @"INTANGIBLES"
#define inventory_key @"INVENTORY"
#define retainedEarnings_key @"RETEARN"
#define totalAssets_key @"ASSETS"
#define sharesOutstanding_key @"SHARESWA"
#define netIncome_key @"NETINC"
#define eps_key @"EPS"
#define dividends_key @"NCFDIV"
#define revenue_key @"REVENUE"
#define cash_key @"CASHNEQ"
#define equity_key @"EQUITY"
#define price_key @"PRICE"


//  Font Name
#define helvetica_neue_thin @"HelveticaNeue-Thin"
#define helvetica_neue @"HelveticaNeue"
#define helvetica_neue_medium @"HelveticaNeue-Medium"
#define helvetica_neue_bold @"HelveticaNeue-Bold"

//  Image Names
#define followButtonImage @"Follow"
#define add @"Add"
#define tick @"Tick"

//  Sizes
#define size_zero 0.0f
#define size_two 2.0f
#define size_three 3.0f
#define size_ten 10.f
#define size_eleven 11.0f
#define size_twelve 12.0f
#define size_thirteen 13.0f
#define size_fourteen 14.0f
#define size_sixteen 16.0f
#define size_eighteen 18.0f
#define size_twentyfour 24.0f

#pragma mark View Controllers

//  ViewController Identifiers
#define lineChartViewController @"lineChartVC"
#define search_tvc_Identifier @"searchTableVC"
#define watchlist_tvc @"watchListTVC"
#define emptyWatchlist_vc @"emptyWatchListVC"
#define tenYearDetailViewController @"tenYearDetailVC"
#define companyDetailVc @"CompanyDetailVC"
#define sectionHeaderID @"sectionHeaderID"

#pragma mark Tableviews & Cells

//  TableviewController Identifiers
#define companyDetailTableHeaderControllerIdentifier @"companyTableHeaderVC"
#define tenYearTableViewControllerIdentifier @"TenYearTableViewControllerIdentifier"
#define tenYearDetailedTableViewControllerIdentifier @"tenYearDetailTableViewControllerIdentifier"

// TableviewCell Identifiers
#define tenYearDetailCellIdentifier @"tenYearDetailedCell"
#define wishListIdentifier @"watchListCell"

//  Section headers
#define oneYearSectionHeader @"oneYearSectionHeader"
#define amountInMillions @"*amount in millions"
#define inMillions @"*in millions"

#pragma mark Segues

//  Segues with Idenifiers
#define companyDetail_tenYearDetail_vc @"tenYearDetailIdentifier"
#define watchlist_pushToCompanyDetailViewController @"watchlistToFinancials"
#define watchlist_pushToSearchViewController @"pushToSearchVC"



#pragma mark Notifications

#define indexChanged @"indexPassed" //use for segmented control index
#define newCompanyData @"newCompany"
@end
