//
//  BaseTableViewController.h
//  projectSnowball
//
//  Created by Donald Fung on 1/15/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Constants.h"
#import "CompanyManager.h"
#import "UIFont+customFonts.h"
#import "UIColor+customColors.h"    

@protocol BaseTableDelegate <NSObject>
@optional
- (void)cellPressed;
- (void)cellPressedWithCompany:(Company *)company;
- (void)refreshPage;
- (void)dismissKeyboard;
- (void)contentSize:(CGFloat)height;
@end

@interface BaseTableViewController : UITableViewController

@property (nonatomic, weak) id <BaseTableDelegate> baseTableDelegate;
@property (strong, nonatomic) CompanyManager *companyManager;

// Top Nav
- (void)setNavBarTitle:(NSString *)title;

// Number formatter
- (NSString *)formatNumberCurrencyStyleForPricesAndRatios:(NSNumber *)number;
- (NSString *)formatNumberCurrencyStyle:(NSNumber *)number;
- (NSString *)formatNumberNoStyle:(NSNumber *)number;

// Set Font
- (UILabel *)customizeLabelsForTenYearTableView:(UILabel *)cellLabel;
- (UIFont *)setFontToHelveticaWithSizeEleven:(UITableViewCell *)cell;
- (UIFont *)setFontToHelveticaWithSizeFourteen:(UITableViewCell *)cell;
- (UIFont *)setFontWithName:(NSString *)name andSize:(CGFloat)size;

// Company Data
- (Company *)getCompanyDataForTableView;

@end
