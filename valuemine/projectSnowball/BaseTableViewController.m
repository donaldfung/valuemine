//
//  BaseTableViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 1/15/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "BaseTableViewController.h"

@implementation BaseTableViewController

//  view life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self adjustTableviewInsets];
}

#pragma mark Top Nav
- (void)setNavBarTitle:(NSString *)title
{
    self.navigationItem.title = title;
}

#pragma mark - Number formatter

- (NSString *)formatNumberCurrencyStyleForPricesAndRatios:(NSNumber *)number
{
    //  Settings for labels that require curreny and decimal formatting
    NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
    [currencyStyle setNumberStyle: NSNumberFormatterCurrencyStyle];
    currencyStyle.maximumFractionDigits = 2;
    NSString *figure = [currencyStyle stringFromNumber:number];
    return figure;
    
}

- (NSString *)formatNumberCurrencyStyle:(NSNumber *)number
{
    float num = [number floatValue]/1000000;
    NSNumber *formattedNum = [NSNumber numberWithFloat:num];
    //  Settings for labels that require curreny and decimal formatting
    NSNumberFormatter *currencyStyle = [[NSNumberFormatter alloc] init];
    [currencyStyle setNumberStyle: NSNumberFormatterCurrencyStyle];
 //   currencyStyle.maximumFractionDigits = 2;
    NSString *figure = [currencyStyle stringFromNumber:formattedNum];
    return figure;
}

- (NSString *)formatNumberNoStyle:(NSNumber *)number
{
    //  Settings for ratios and other non currency strings
    NSNumberFormatter *normalStyle = [[NSNumberFormatter alloc] init];
    [normalStyle setNumberStyle: NSNumberFormatterNoStyle];
    normalStyle.maximumFractionDigits = 2;
    NSString *figure = [normalStyle stringFromNumber:number];
    return figure;
}

#pragma mark Set Font

- (UILabel *)customizeLabelsForTenYearTableView:(UILabel *)cellLabel
{
    //  Standard settings for labels.  Override in some other method
    cellLabel.font = [UIFont fontWithName:helvetica_neue_medium size:size_thirteen];
    cellLabel.textColor = [UIColor darkGrayColor];
    return cellLabel;
}

- (UIFont *)setFontToHelveticaWithSizeEleven:(UITableViewCell *)cell
{
    cell.textLabel.font = [UIFont fontWithName:helvetica_neue size:size_eleven];
    return cell.textLabel.font;
}


- (UIFont *)setFontToHelveticaWithSizeFourteen:(UITableViewCell *)cell
{
    cell.textLabel.font = [UIFont fontWithName:helvetica_neue size:size_fourteen];
    return cell.textLabel.font;
}


- (UIFont *)setFontWithName:(NSString *)name andSize:(CGFloat)fontSize
{
    UILabel *label;
    label.font = [UIFont fontWithName:name size:fontSize];
    return label.font;
}

#pragma mark Company Data

- (Company *)getCompanyDataForTableView
{
    self.companyManager = [CompanyManager sharedCompany];
    Company *company = self.companyManager.company;
    return  company;
}

- (void)adjustTableviewInsets
{
    [self.tableView setSeparatorInset:UIEdgeInsetsMake(0, size_ten, 0, size_ten)];
}

#pragma mark - Table view delegate


@end
