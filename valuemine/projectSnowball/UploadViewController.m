//
//  UploadViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 1/2/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "UploadViewController.h"
#import "ServerClient.h"
#import "WatchListViewController.h"
#import "LocalNotificationsManager.h"
#import "Constants.h"


typedef NS_ENUM(int, time_delay)
{
    one_sec = 1,
    two_secs = 2,
    three_secs = 3,
    four_secs = 4,
    five_secs = 5,
    six_secs = 6,
    seven_secs = 7,
    ten_secs = 10
};

@interface UploadViewController ()

@property (nonatomic, strong) ServerClient *serverClient;
@property (nonatomic, strong) LocalNotificationsManager *localNotification;
@property (nonatomic, strong) NSMutableArray *tickerKeys;
@property (nonatomic, strong) NSTimer *timer;
@property (strong, nonatomic) IBOutlet UIButton *uploadCurrentPriceButton;
@property (strong, nonatomic) IBOutlet UIButton *stopButton;
@property (strong, nonatomic) IBOutlet UIButton *uploadButton;

@end

@implementation UploadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.stopButton.hidden = YES;
    _serverClient = [[ServerClient alloc]init];
    [self preloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self launchedWithNotification];
}

- (void)launchedWithNotification
{
    self.localNotification = [LocalNotificationsManager localNotificationsManager];
    if (self.localNotification.notificationOpen) {
        [self uploadPriceWithAlert];
    }
}


#pragma mark Buttons

- (IBAction)uploadPressed:(id)sender {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Caution"
                                                                   message:@"Upload about to begin"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* uploadAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             NSLog(@"Upload Process Started");
                                                             self.uploadButton.hidden = YES;
                                                             self.stopButton.hidden = NO;
                                                             self.uploadCurrentPriceButton.hidden = YES;
                                                             self.timer = [NSTimer scheduledTimerWithTimeInterval:five_secs
                                                                                                           target:self
                                                                                                         selector:@selector(timerStarted)
                                                                                                         userInfo:nil
                                                                                                          repeats:YES];
                                                             
                                                         }];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             
                                                         }];
    
    [alert addAction:cancelAction];
    [alert addAction:uploadAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)uploadCurrentPrice:(id)sender
{
    [self uploadPriceWithAlert];
}

- (void)uploadPriceWithAlert
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Caution"
                                                                   message:@"About to update the database with current prices.  Proceed?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* uploadAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             NSLog(@"Update Process Started");
                                                             self.uploadCurrentPriceButton.hidden = YES;
                                                             self.stopButton.hidden = NO;
                                                             self.uploadButton.hidden = YES;
                                                             self.timer = [NSTimer scheduledTimerWithTimeInterval:three_secs
                                                                                                           target:self
                                                                                                         selector:@selector(timerForUpdatingPrices)
                                                                                                         userInfo:nil
                                                                                                          repeats:YES];
                                                             self.localNotification.notificationOpen = NO;
                                                         }];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             self.localNotification.notificationOpen = NO;
                                                         }];
    
    [alert addAction:cancelAction];
    [alert addAction:uploadAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)stopPressed:(id)sender {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Caution"
                                                                   message:@"Are you sure you want to stop uploading?"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* uploadAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             NSLog(@"Upload Process Stopped");
                                                             self.uploadButton.hidden = NO;
                                                             self.uploadCurrentPriceButton.hidden = NO;
                                                             self.stopButton.hidden = YES;
                                                             [self.timer invalidate];
                                                             self.timer = nil;
                                                             
                                                         }];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action) {
                                                             
                                                         }];
    
    [alert addAction:cancelAction];
    [alert addAction:uploadAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)preloadData
{
    _tickerKeys = [[NSMutableArray alloc]init];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
        NSString *dbPathStringForNYSE;
        NSString *dbPathStringForNASDAQ;
        NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        NSString *docPath = [path objectAtIndex:0];
        dbPathStringForNYSE = [docPath stringByAppendingPathComponent:nyse];
        dbPathStringForNASDAQ = [docPath stringByAppendingPathComponent:nasdaq];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL success;
        success = [fileManager fileExistsAtPath:dbPathStringForNYSE];
        if(!success){
            NSString *filePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:nyse];
            [fileManager copyItemAtPath:filePathFromApp toPath:dbPathStringForNYSE  error:nil];
        }
        BOOL inThere;
        if(!inThere){
            NSString *filePathFromApp2 = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:nasdaq];
            
            [fileManager copyItemAtPath:filePathFromApp2 toPath:dbPathStringForNASDAQ  error:nil];
        }
        NSString *readNYSE = [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:dbPathStringForNYSE] encoding:NSUTF8StringEncoding];
        NSString *readNASDAQ = [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:dbPathStringForNASDAQ] encoding:NSUTF8StringEncoding];
        NSString *stringOfCompanies = [readNYSE stringByAppendingString:readNASDAQ];
        NSArray *companyArrays = [stringOfCompanies componentsSeparatedByString:@"\n"];
        for (int i = 0; i < [companyArrays count] - 1; i ++)
        {
            NSString *readEachElement = [companyArrays objectAtIndex:i];
            NSArray *separateTickerFromCompany = [readEachElement componentsSeparatedByString:@"\t"];
            [self.tickerKeys addObject:separateTickerFromCompany];
        }
    });
}

#pragma mark Parse Timer

- (void)timerStarted
{
    if (self.tickerKeys.count > 0) {
        NSArray *company = [self.tickerKeys lastObject];
        NSString *ticker = [company objectAtIndex:0];
        NSString *name = [company objectAtIndex:1];
        [self.serverClient checkIfCompanyExistsWithName:name andTicker:ticker];
        [self.tickerKeys removeObject:company];
    }
    else {
        [self invalidateTimer];
    }
}

- (void)timerForUpdatingPrices
{
    if (self.tickerKeys.count > 0) {
        NSArray *company = [self.tickerKeys lastObject];
        NSString *ticker = [company objectAtIndex:0];
        [self.serverClient updateCompanyWithTicker:ticker];
        [self.tickerKeys removeObject:company];
    }
    else {
        [self invalidateTimer];
    }
}

- (void)invalidateTimer
{
    [self.timer invalidate];
    self.timer = nil;
    NSLog(@"Upload Process Complete!");
}

#pragma mark - Main Screen Button
- (IBAction)goToMainScreenClicked:(id)sender
{
    
}

@end
