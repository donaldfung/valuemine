//
//  SearchViewControllerCells.h
//  projectSnowball
//
//  Created by Donald Fung on 11/26/15.
//  Copyright © 2015 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewControllerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *companyTicker;
@property (weak, nonatomic) IBOutlet UILabel *companyName;
- (IBAction)saveButton:(UIButton *)sender;

@end
