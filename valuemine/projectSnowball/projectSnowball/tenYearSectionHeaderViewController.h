//
//  tenYearSectionHeaderViewController.h
//  projectSnowball
//
//  Created by Donald Fung on 2/18/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface tenYearSectionHeaderViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *leftLabel;
@property (strong, nonatomic) IBOutlet UILabel *rightLabel;
@end
