//
//  ServerClient.h
//  projectSnowball
//
//  Created by Donald Fung on 12/27/15.
//  Copyright © 2015 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <Parse/Parse.h>
#import "CompanyDetailViewController.h"

extern NSString* const parseClass;
extern NSString* const ticker_parse;
extern NSString* const name_parse;
extern NSString* const company_parse;
extern NSString* const current_price;
extern NSString* const ratios_parse;

@interface ServerClient : NSObject

+ (ServerClient *)sharedClient;
//  User Side Methods
- (void)fetchCompanyWithName:(NSString *)companyName andTicker:(NSString *)stockTicker;

//  Developer Side Methods
- (void)checkIfCompanyExistsWithName:(NSString *)companyName andTicker:(NSString *)stockTicker;
- (void)updateCompanyWithTicker:(NSString *)ticker;
@end
