//
//  LineChartViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 1/24/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "LineChartViewController.h"
#import <UIKit/UIKit.h>
#import "UIColor+customColors.h"
#import "TenYearDetailTableViewController.h"

@interface LineChartViewController () <BEMSimpleLineGraphDataSource, BEMSimpleLineGraphDelegate>

@property (weak, nonatomic) IBOutlet UIView *graphHolder;
@property (weak, nonatomic) IBOutlet UIView *dateSection;
@property (weak, nonatomic) IBOutlet UILabel *firstDate;
@property (weak, nonatomic) IBOutlet UILabel *lastDate;
@end

@implementation LineChartViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureLineChart];
    [self configureDateSection];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

#pragma mark Line Chart

- (void)configureLineChart
{
    //  initialize line graph objct
    self.lineGraph = [[BEMSimpleLineGraphView alloc]initWithFrame:CGRectMake(0, 0, screen_width, self.graphHolder.frame.size.height)];
    
    //  set datasource and delegate
    self.lineGraph.dataSource = self;
    self.lineGraph.delegate = self;
    
    //  set background color
    self.lineGraph.backgroundColor = [UIColor darkBlue];
    self.graphHolder.backgroundColor = [UIColor darkBlue];
    
    //  other settings
    self.lineGraph.animationGraphStyle = BEMLineAnimationNone;
    self.lineGraph.colorTop = [UIColor darkBlue];
    self.lineGraph.colorBottom = [UIColor darkBlue];
    self.lineGraph.enableBezierCurve = YES;
    self.lineGraph.widthLine = size_two;
    self.lineGraph.enableTouchReport = YES;
    self.lineGraph.enablePopUpReport = YES;
    
    //  Y axis
    // self.lineGraph.enableYAxisLabel = YES;
    self.lineGraph.colorYaxisLabel = [UIColor whiteColor];
    self.lineGraph.autoScaleYAxis = YES;
    self.lineGraph.labelFont = [UIFont fontWithName:helvetica_neue size:size_ten];
    //  self.lineGraph.positionYAxisRight = YES;
    
    //  X axis
    //self.lineGraph.enableXAxisLabel = YES;
    // self.lineGraph.colorXaxisLabel = [UIColor whiteColor];
    // self.lineGraph.enableReferenceAxisFrame = YES;
    //    self.lineGraph.enableReferenceXAxisLines = YES;
    //    self.lineGraph.enableReferenceYAxisLines = YES;
    
    [self.graphHolder addSubview:self.lineGraph];
}

#pragma mark Line Chart data source

- (NSInteger)numberOfPointsInLineGraph:(BEMSimpleLineGraphView *)graph
{
    return self.graphData.count; // Number of points in the graph.
}

#pragma mark Line Chart Delegate

- (CGFloat)lineGraph:(BEMSimpleLineGraphView *)graph valueForPointAtIndex:(NSInteger)index
{
    //  Reverse order of array to plot oldest data first
    NSArray* reversedArray = [[self.graphData reverseObjectEnumerator] allObjects];
    NSArray *data = [reversedArray objectAtIndex:index];
    
    //  Get figure from array
    NSNumber *figure = [data objectAtIndex:1];
    
    //  convert it to a format readable by the graph
    CGFloat y = [figure doubleValue];
    
    return y; // The value of the point on the Y-Axis for the index.
}

//- (nullable NSString *)lineGraph:(nonnull BEMSimpleLineGraphView *)graph labelOnXAxisForIndex:(NSInteger)index
//{
//    NSArray* reversedArray = [[self.graphData reverseObjectEnumerator] allObjects];
//    
//    //  display first object
//    NSArray *data = [reversedArray objectAtIndex:0];
//    NSString *date = [data objectAtIndex:0];
//    return date;
//}

#pragma mark Dates

- (void)configureDateSection
{
    self.dateSection.backgroundColor = [UIColor darkBlue];
    [self getValueForFirstDate];
    [self getValueForLastDate];
}

- (void)getValueForFirstDate
{
    NSArray* reversedArray = [[self.graphData reverseObjectEnumerator] allObjects];
    NSArray *data = [reversedArray objectAtIndex:0];
    
    //  First Date
    NSString *date = [data objectAtIndex:0];
    self.firstDate.text = [self getYear:date];
    self.firstDate.textColor = [UIColor whiteColor];
    self.firstDate.textAlignment = NSTextAlignmentLeft;
    self.firstDate.font = [self setFontToHelveticaWithSizeSixteen];
}

- (void)getValueForLastDate
{
    NSArray* reversedArray = [[self.graphData reverseObjectEnumerator] allObjects];
    NSArray *data = [reversedArray lastObject];
    
    //  Last Date
    NSString *date = [data objectAtIndex:0];
    self.lastDate.text = [self getYear:date];
    self.lastDate.textColor = [UIColor whiteColor];
    self.lastDate.textAlignment = NSTextAlignmentRight;
    self.lastDate.font = [self setFontToHelveticaWithSizeSixteen];
}

@end
