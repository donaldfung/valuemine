//
//  TenYearDetailTableViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 1/23/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "TenYearDetailTableViewController.h"
#import "Constants.h"
#import "TenYearDetailedTableViewCell.h"
#import "UIColor+customColors.h"
#import "tenYearSectionHeaderViewController.h"

@interface TenYearDetailTableViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) TenYearDetailedTableViewCell *cell;
@property (nonatomic, strong) tenYearSectionHeaderViewController *sectionHeader;
@end

@implementation TenYearDetailTableViewController

#pragma mark View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    if ([self.baseTableDelegate respondsToSelector:@selector(contentSize:)])
    {
        [self.baseTableDelegate contentSize:self.tableView.contentSize.height];
    }
}
#pragma mark Tableview Datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableFigures count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.cell = [tableView dequeueReusableCellWithIdentifier:tenYearDetailCellIdentifier];
    
    if (self.cell == nil)
    {
        self.cell = [tableView dequeueReusableCellWithIdentifier:tenYearDetailCellIdentifier];
    }
    [self.cell dataForCell:self.tableFigures withSectionTitle:self.sectionTitle andIndexPath: indexPath];
    
    //  turn off selection
    [self.cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return self.cell;
}

#pragma mark Tableview Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CompanyDetail_Cell_Height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    self.sectionHeader = [self.storyboard instantiateViewControllerWithIdentifier:sectionHeaderID];
    self.sectionHeader.view.backgroundColor = [UIColor lightBlue];
    [self customizelabel:self.sectionHeader.leftLabel withAlignment:NSTextAlignmentLeft andText:self.sectionTitle andFontSize:size_sixteen];
    [self customizelabel:self.sectionHeader.rightLabel withAlignment:NSTextAlignmentRight andText:inMillions andFontSize:size_thirteen];
    return self.sectionHeader.view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CompanyDetail_Cell_Height;
}

- (void)customizelabel:(UILabel *)label withAlignment:(NSTextAlignment)alignment andText:(NSString *)text andFontSize:(float)fontSize
{
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = alignment;
    label.text = text;
    label.font = [UIFont fontWithName:helvetica_neue size:fontSize];
    label.textColor = [UIColor defaultTheme];
}


@end
