//
//  SlideOneViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 1/17/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "SlideOneViewController.h"
#import "Company.h"
#import "UIColor+customColors.h"

@interface SlideOneViewController ()
@property (weak, nonatomic) IBOutlet UIView *rowFour;
@property (weak, nonatomic) IBOutlet UIView *rowThree;
@property (weak, nonatomic) IBOutlet UIView *rowTwo;
@property (weak, nonatomic) IBOutlet UIView *rowOne;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *peRatioLabel;
@property (weak, nonatomic) IBOutlet UILabel *peRatioFigure;
@property (weak, nonatomic) IBOutlet UILabel *pegLabel;
@property (weak, nonatomic) IBOutlet UILabel *pegFigure;
@property (weak, nonatomic) IBOutlet UILabel *debtToCapLabel;
@property (weak, nonatomic) IBOutlet UILabel *debtToCapFigure;
@property (weak, nonatomic) IBOutlet UILabel *pbLabel;
@property (weak, nonatomic) IBOutlet UILabel *pbFigure;
@property (weak, nonatomic) IBOutlet UILabel *currentRatioLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentRatioFigure;
@property (weak, nonatomic) IBOutlet UILabel *quickRatioLabel;
@property (weak, nonatomic) IBOutlet UILabel *quickRatioFigure;
@property (weak, nonatomic) IBOutlet UILabel *epsLabel;
@property (weak, nonatomic) IBOutlet UILabel *epsFigure;
@property (weak, nonatomic) IBOutlet UILabel *divPerShareLabel;
@property (weak, nonatomic) IBOutlet UILabel *divPerShareFigure;
@end

@implementation SlideOneViewController

- (void)viewDidLoad
{
    self.companyManager = [CompanyManager sharedCompany];
    [self setValuesForLabels];
    [self setBackgroundColor];
    [self configureLabels];
}

#pragma mark Configure UI

- (void)setBackgroundColor
{
    self.rowOne.backgroundColor = [UIColor darkBlue];
    self.rowTwo.backgroundColor = [UIColor darkBlue];
    self.rowThree.backgroundColor = [UIColor darkBlue];
    self.rowFour.backgroundColor = [UIColor darkBlue];
    self.contentView.backgroundColor = [UIColor darkBlue];
}

- (void)configureLabels
{
    [self customizeLabelForSummaryView:self.peRatioLabel];
    [self customizeLabelForSummaryView:self.peRatioFigure];
    [self customizeLabelForSummaryView:self.pegLabel];
    [self customizeLabelForSummaryView:self.pegFigure];
    [self customizeLabelForSummaryView:self.debtToCapFigure];
    [self customizeLabelForSummaryView:self.debtToCapLabel];
    [self customizeLabelForSummaryView:self.pbLabel];
    [self customizeLabelForSummaryView:self.pbFigure];
    [self customizeLabelForSummaryView:self.currentRatioFigure];
    [self customizeLabelForSummaryView:self.currentRatioLabel];
    [self customizeLabelForSummaryView:self.quickRatioFigure];
    [self customizeLabelForSummaryView:self.quickRatioLabel];
    [self customizeLabelForSummaryView:self.epsFigure];
    [self customizeLabelForSummaryView:self.epsLabel];
    [self customizeLabelForSummaryView:self.divPerShareFigure];
    [self customizeLabelForSummaryView:self.divPerShareLabel];
    
}

#pragma mark Data for view
- (void)setValuesForLabels
{
    //  Numbers to strings for view
    Company *company = [self getCompanyDataForViewController];
    NSString *peRatio = [self formatNumberNoStyle:company.priceToEarningsRatio];
    NSString *pegRatio = [self formatNumberNoStyle:company.pegRatio];
    NSString *debtToCapRatio = [self formatNumberNoStyle:company.debtToCapitalRatio];
    NSString *pbRatio = [self formatNumberNoStyle:company.priceToBookRatio];
    NSString *currentRatio = [self formatNumberNoStyle:company.currentRatio];
    NSString *quickRatio = [self formatNumberNoStyle:company.quickRatio];
    NSString *eps = [self formatNumberCurrencyStyle:company.earningsPerShare];
    NSString *divPerShare = [self formatNumberCurrencyStyle:company.dividendsPerShare];
    
    //  Assign values to labels for view
    self.peRatioLabel.text = @"P/E";
    self.peRatioFigure.text = peRatio;
    self.pegLabel.text = @"PEG";
    self.pegFigure.text = pegRatio;
    self.debtToCapFigure.text = debtToCapRatio;
    self.debtToCapLabel.text = @"D/C Ratio";
    self.pbFigure.text = pbRatio;
    self.pbLabel.text = @"P/B";
    self.currentRatioFigure.text = currentRatio;
    self.currentRatioLabel.text = @"Current Ratio";
    self.quickRatioFigure.text = quickRatio;
    self.quickRatioLabel.text = @"Quick Ratio";
    self.epsFigure.text = eps;
    self.epsLabel.text = @"EPS";
    self.divPerShareFigure.text = divPerShare;
    self.divPerShareLabel.text = @"DPS";
}



@end
