//
//  WatchListTableViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 1/15/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WatchListTableViewController.h"
#import "WishListTableViewCell.h"
#import "Company.h"
#import "CoreDataManager.h"

@interface WatchListTableViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) WishListTableViewCell *wishListCell;

@end
@implementation WatchListTableViewController

#pragma mark View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark Configure UI

- (void)customizeCells
{
    //  set label fonts and colors
    
    self.wishListCell.figureForCell.textColor = [UIColor defaultTheme];
    self.wishListCell.figureForCell.font = [UIFont fontWithName:helvetica_neue size:size_sixteen];
    self.wishListCell.tickerSymbolLabel.textColor = [UIColor defaultTheme];
    self.wishListCell.tickerSymbolLabel.font = [UIFont fontWithName:helvetica_neue size:size_sixteen];
}

#pragma mark Table View Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.savedCompanies.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.wishListCell = [tableView dequeueReusableCellWithIdentifier:wishListIdentifier forIndexPath:indexPath];
    if (self.wishListCell == nil) {
        self.wishListCell = [tableView dequeueReusableCellWithIdentifier:wishListIdentifier];
    }
    self.wishListCell.accessoryType = UITableViewCellAccessoryNone;
    Company *savedCompanies = [self.savedCompanies objectAtIndex:indexPath.row];
    self.wishListCell.tickerSymbolLabel.text = savedCompanies.ticker;
    [self setValuesForCells:savedCompanies];
    
    return self.wishListCell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Company *company = [self.savedCompanies objectAtIndex:indexPath.row];

    if ([self.baseTableDelegate respondsToSelector:@selector(cellPressedWithCompany:)]) {
        [self.baseTableDelegate cellPressedWithCompany:company];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        Company *companySelected = [self.savedCompanies objectAtIndex:indexPath.row];
        [[CoreDataManager coreDataManager] deleteCompany:companySelected];
        [self.savedCompanies removeObjectAtIndex:indexPath.row];
        [self.tableView reloadData];
        /*  if no companies exist in watchlist, tell delegate to load the empty watchlist view.  Otherwise, reload the table with the updated data */
        if (self.savedCompanies.count == 0) {
            if ([self.baseTableDelegate respondsToSelector:@selector(refreshPage)]) {
                [self.baseTableDelegate refreshPage];
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return Watchlist_Cell_Height;
}

#pragma mark Segmented Index Check

- (void)setValuesForCells:(Company *)savedCompanies
{
    //  set values for labels
    [self customizeCells];
    
    if (self.segmentedIndex == 0)
    {
        NSString *stockPrice = [self formatNumberCurrencyStyleForPricesAndRatios:savedCompanies.stockPrice];
        self.wishListCell.figureForCell.text = stockPrice;
    }
    else if (self.segmentedIndex == 1)
    {
        NSString *peRatio = [self formatNumberNoStyle:savedCompanies.priceToEarningsRatio];
        self.wishListCell.figureForCell.text = peRatio;
    }
    else if (self.segmentedIndex == 2)
    {
        NSString *pbRatio = [self formatNumberNoStyle:savedCompanies.priceToBookRatio];
        self.wishListCell.figureForCell.text = pbRatio;
    }
}


@end
