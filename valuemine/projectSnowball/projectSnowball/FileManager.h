//
//  FileManager.h
//  projectSnowball
//
//  Created by Donald Fung on 2/15/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject

+ (id)sharedFileManager;
- (NSMutableDictionary *)saveQuandlList;
- (void)saveToDocumentsDirectory:(NSString *)fileName;
- (NSString *)documentPath:(NSString *)fileName;

@end
