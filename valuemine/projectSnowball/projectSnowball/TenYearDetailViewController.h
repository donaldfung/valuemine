//
//  TenYearDetailViewController.h
//  projectSnowball
//
//  Created by Donald Fung on 1/30/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "BaseViewController.h"

@interface TenYearDetailViewController : BaseViewController

@property (nonatomic, strong) NSArray *tenYearData;
@property (nonatomic, strong) NSString *sectionTitle;
@property (nonatomic, strong) NSString *topNavTitle;
@end
