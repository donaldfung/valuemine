//
//  LocalNotificationsManager.m
//  projectSnowball
//
//  Created by Donald Fung on 1/10/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "LocalNotificationsManager.h"
#import "Constants.h"
#import "UploadViewController.h"

typedef NS_ENUM(int, numberOfHoursOrMin) {
    zero,
    one = 1,
    two = 2,
    three = 3,
    four = 4,
    five = 5,
    six = 6,
    thirteen = 13,
    fourteen = 14,
    nineteen = 19,
    twentyone = 21,
    fortySeven = 47,
    fifty = 50
};

@implementation LocalNotificationsManager

+ (id)localNotificationsManager
{
    static LocalNotificationsManager *localNotificationsManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        localNotificationsManager = [[self alloc] init];
    });
    return localNotificationsManager;
}

- (id)init {
    if (self = [super init]) {

    }
    return self;
}

- (void)scheduleNotification
{
    NSDate *specifiedTime = [self formatTime];
    // Schedule the notification
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    if (localNotification == nil)
        return;
    localNotification.fireDate = specifiedTime;
    localNotification.alertBody = @"time to update current prices!";
    localNotification.alertAction = @"Snowball";
    localNotification.timeZone = [NSTimeZone localTimeZone];
    //localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (void)launchAppWithLocalNotification:(UIApplication *)application andLocalNotificastion:(UILocalNotification *)localNotification
{
    //  Notification received in the foreground
    UIApplicationState state = [application applicationState];
//    if (state == UIApplicationStateActive) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reminder"
//                                                        message:localNotification.alertBody
//                                                       delegate:self cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
//    }
    
    //  Notification received in the background
    if (state == UIApplicationStateBackground || state == UIApplicationStateInactive) {
        self.notificationOpen = YES;
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[[NSBundle mainBundle].infoDictionary objectForKey:@"UIMainStoryboardFile"] bundle:[NSBundle mainBundle]];;
        UIWindow *window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
        UINavigationController *navController = (UINavigationController *)window.rootViewController;
        UploadViewController *uploadVC = [storyboard instantiateViewControllerWithIdentifier:@"UploadVCIdentifier"];
        [navController pushViewController:uploadVC animated:YES];
    }
    
    // Set icon badge number to zero
    application.applicationIconBadgeNumber = 0;
}


#pragma mark - Date

- (NSDate *)formatTime
{
    //  Days, Months, Year
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    [components setHour:twentyone];
    [components setMinute:zero];
    [components setSecond:zero];
    NSDate *itemDate = [calendar dateFromComponents:components];
    return itemDate;
}

@end
