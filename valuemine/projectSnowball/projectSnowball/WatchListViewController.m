//
//  ViewController.m
//  projectSnowball
//
//  Created by Donald Fung & Blayne Cameron on 9/19/15.
//  Copyright (c) 2015 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WatchListViewController.h"
#import "SearchViewController.h"
#import "CompanyDetailViewController.h"
#import "WatchListTableViewController.h"
#import "EmptyWatchListViewController.h"
#import "DVSwitch.h"
#import "ServerClient.h"
#import "Flurry.h"

typedef NS_ENUM(int, segmentedIndex)
{
    price_button = 0,
    peRatio_button = 1,
    pbRatio_button = 2
};

@interface WatchListViewController () <BaseTableDelegate, BaseViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *segmentedControlView;
@property (nonatomic, strong) NSMutableArray *fetchedCompanies;
@property (weak, nonatomic) IBOutlet UIView *tableviewHolder;
@property (nonatomic, strong) WatchListTableViewController *watchlistTVC;
@property (nonatomic, strong) EmptyWatchListViewController *emptyWatchListVC;
@property (nonatomic, assign) segmentedIndex index_selected;
@property (nonatomic, strong) DVSwitch *switcher;
@end

@implementation WatchListViewController

#pragma mark - View Events

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self topNavBar];
    [self configureSegmentedControl];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self fetchSavedCompanies];
    [self configureNotificationForCompanyData];
}

- (void)viewDidLayoutSubviews
{
    
}

#pragma mark Configure UI

- (void)topNavBar
{
    [self setNavBarTitle:watclist_title];
}

- (void)configureSegmentedControl
{
    //  initialize segmented control
    self.switcher = [[DVSwitch alloc] initWithStringsArray:@[@"Price", @"P/E", @"P/B"]];
    self.switcher.frame = CGRectMake(0, 0, self.segmentedControlView.frame.size.width, self.segmentedControlView.frame.size.height);
    
    //  configure visual settings
    self.headerView.backgroundColor = [UIColor darkBlue];
    self.segmentedControlView.backgroundColor = [UIColor darkBlue];
    self.switcher.backgroundColor = [UIColor segmentedControlBackground];
    self.switcher.labelTextColorInsideSlider = [UIColor darkBlue];
    self.switcher.labelTextColorOutsideSlider = [UIColor whiteColor];
    self.switcher.cornerRadius = size_fourteen;
    self.switcher.sliderOffset = size_zero;
    self.switcher.font = [UIFont fontWithName:helvetica_neue_medium size:size_thirteen];
    
    //  use a weak type of self to prevent a strong retention within the block
    __weak typeof(self) weakSelf = self;
    [self.segmentedControlView addSubview:self.switcher];
    
    //  this block gets called when the segmented control is tapped
    [self.switcher setPressedHandler:^(NSUInteger index) {
        
        [weakSelf tabTappedWithIndex:index];
        
        [Flurry logEvent:@"Segmented_Control_Selected"
          withParameters:@{@"index":[NSString stringWithFormat:@"%lu", index]}];
        
        //  reload the tableview
        if (weakSelf.watchlistTVC != nil) {
            [weakSelf.watchlistTVC.tableView reloadData];
        }
    }];
}

- (void)loadViewsToScreen
{
    if (self.fetchedCompanies.count == 0) {
        //  add placeholder view
        dispatch_async(dispatch_get_main_queue(), ^{
             [self loadBlankView];
        });
    }
    else if (self.fetchedCompanies.count > 0)
    {
        [self loadWatchListTableView];
    }
}

- (void)loadWatchListTableView
{
    
    if (self.watchlistTVC == nil) {
        self.tableviewHolder.frame = CGRectMake(0, 45, screenWidth, screenHeight);
        self.watchlistTVC = [self.storyboard instantiateViewControllerWithIdentifier:watchlist_tvc];
        self.watchlistTVC.baseTableDelegate = self;
        self.watchlistTVC.view.frame = CGRectMake(0, 0, screenWidth, screenHeight);
        
    }
    
    [self.tableviewHolder addSubview:self.watchlistTVC.view];
    
    self.watchlistTVC.savedCompanies = self.fetchedCompanies;
    [self.watchlistTVC.tableView reloadData];
}

- (void)loadBlankView
{
    //  Empty Watchlist
    if (self.emptyWatchListVC == nil) {
        self.emptyWatchListVC = [self.storyboard instantiateViewControllerWithIdentifier:emptyWatchlist_vc];
        self.emptyWatchListVC.baseViewDelegate = self;
    }
    [self.tableviewHolder addSubview:self.emptyWatchListVC.view];
}

- (void)configureNotificationForCompanyData
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processNewCompanyData)
                                                 name:newCompanyData
                                               object:nil];
}

- (void)processNewCompanyData
{
    [self fetchSavedCompanies];
}


#pragma mark Segmented Control Setup and Handle Tap

- (void)tabTappedWithIndex:(NSUInteger)index
{
    switch (index) {
        case price_button:
            [self selectedIndex:price_button];
            break;
        case peRatio_button:
            [self selectedIndex:peRatio_button];
            break;
        case pbRatio_button:
            [self selectedIndex:pbRatio_button];
            break;
        default:
            break;
    }
}

- (void)selectedIndex:(int)index
{
    self.index_selected = index;
    //  send values to tableview
    self.watchlistTVC.segmentedIndex = self.index_selected;
}

#pragma mark - Fetch saved companies
- (void)fetchSavedCompanies
{
    self.fetchedCompanies = [[CoreDataManager coreDataManager] fetchCompanies];
    [self loadViewsToScreen];
}

#pragma mark Buttons
- (IBAction)backPressed:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:NO];
    });
}

#pragma mark Base View Controller Delegate

- (void)transitionToViewController
{
    [self performSegueWithIdentifier:watchlist_pushToSearchViewController sender:self];
}

#pragma mark Base Table Delegate

- (void)cellPressedWithCompany:(Company *)company
{
    [[CompanyManager sharedCompany]prepareRequest:company];
    [self performSegueWithIdentifier:watchlist_pushToCompanyDetailViewController sender:self];
}

- (void)refreshPage
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self loadBlankView];
    });
    
}

@end
