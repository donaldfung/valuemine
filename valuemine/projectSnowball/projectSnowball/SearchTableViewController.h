//
//  SearchTableViewController.h
//  projectSnowball
//
//  Created by Donald Fung on 1/16/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "BaseTableViewController.h"

@interface SearchTableViewController : BaseTableViewController

@property (nonatomic, strong)NSArray *results;
@end
