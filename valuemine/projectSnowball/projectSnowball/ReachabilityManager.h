//
//  ReachabilityManager.h
//  projectSnowball
//
//  Created by Donald Fung on 2/18/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Reachability;
@interface ReachabilityManager : NSObject

@property (strong, nonatomic) Reachability *reachability;

#pragma mark Shared Manager
+ (ReachabilityManager *)sharedManager;

#pragma mark Class Methods
+ (BOOL)isReachable;

@end
