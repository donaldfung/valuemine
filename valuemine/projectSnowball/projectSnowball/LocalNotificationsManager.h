//
//  LocalNotificationsManager.h
//  projectSnowball
//
//  Created by Donald Fung on 1/10/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface LocalNotificationsManager : NSObject

+ (id)localNotificationsManager;
- (void)scheduleNotification;
- (void)launchAppWithLocalNotification:(UIApplication *)application andLocalNotificastion:(UILocalNotification *)localNotification;

@property BOOL notificationOpen;
@end
