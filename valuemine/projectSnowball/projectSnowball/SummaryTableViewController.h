//
//  SummaryTableViewController.h
//  projectSnowball
//
//  Created by Donald Fung on 1/17/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "BaseTableViewController.h"
//#import "SlideOneViewController.h"
//#import "SlideTwoViewController.h"

#define slideOneID @"slideOneIdentifier"
#define slideTwoID @"slideTwoVcIdentifier"
#define contentHeight 180
@interface SummaryTableViewController : BaseTableViewController 
//@property (nonatomic, strong) SlideOneViewController *slideOneVC;
//@property (nonatomic, strong) SlideTwoViewController *slideTwoVC;
@end

