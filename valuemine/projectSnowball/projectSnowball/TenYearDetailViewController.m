//
//  TenYearDetailViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 1/30/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "TenYearDetailViewController.h"
#import "TenYearDetailTableViewController.h"
#import "LineChartViewController.h"


@interface TenYearDetailViewController () <UIScrollViewDelegate, BaseTableDelegate>

@property (weak, nonatomic) IBOutlet UIView *chartHolder;
@property (weak, nonatomic) IBOutlet UIView *tableviewHolder;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) TenYearDetailTableViewController *tenYearDetailTableViewController;
@property (nonatomic, strong) LineChartViewController *chartViewController;
@property (weak, nonatomic) IBOutlet UIView *backdropView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *followButton;

@end

@implementation TenYearDetailViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadChartview];
    [self loadTableView];
    [self configureUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self changeStateOfFollowButton];
}

#pragma mark Configure UI

- (void)configureUI
{
    [self setNavBarTitle:self.topNavTitle];
    self.backdropView.backgroundColor = [UIColor darkBlue];
}

#pragma mark Subviews
- (void)loadChartview
{
    self.chartViewController = [self.storyboard instantiateViewControllerWithIdentifier:lineChartViewController];
    self.chartViewController.graphData = self.tenYearData;
    self.chartViewController.view.frame = CGRectMake(0,0, self.chartHolder.frame.size.width, self.chartHolder.frame.size.height);
    [self.chartHolder addSubview:self.chartViewController.view];
    [self.chartViewController.lineGraph reloadGraph];
}

- (void)loadTableView
{
    self.tenYearDetailTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:tenYearDetailedTableViewControllerIdentifier];
    self.tenYearDetailTableViewController.tableFigures = self.tenYearData;
    self.tenYearDetailTableViewController.sectionTitle = self.sectionTitle;
    self.tenYearDetailTableViewController.baseTableDelegate = self;
    [self.tableviewHolder addSubview:self.tenYearDetailTableViewController.view];
    self.tenYearDetailTableViewController.tableView.scrollEnabled = NO;
}

#pragma mark Base Table Delegate

- (void)contentSize:(CGFloat)height
{
    [self getScrollViewContentSize:height];
}

#pragma mark Scroll View

- (UIScrollView *)getScrollViewContentSize:(CGFloat)tableHeight
{
    CGFloat contentSize = self.chartHolder.frame.size.height + tableHeight;
    self.scrollView.contentSize = CGSizeMake(screen_width, contentSize);
    self.tenYearDetailTableViewController.view.frame = CGRectMake(0,0, screenWidth, contentSize);
    return self.scrollView;
}

#pragma mark Buttons

- (IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Follow company

- (IBAction)addButtonPressed:(id)sender
{
    Company *company = [self getCompanyDataForViewController];
    if (company.is_following)
    {
        [[CoreDataManager coreDataManager] deleteCompany:company];
        self.companyManager = [CompanyManager sharedCompany];
        self.companyManager.company.is_following = NO;
        [self changeButtonToAdd];
    }
    else
    {
        [[CoreDataManager coreDataManager] saveCompany:company];
        self.companyManager.company.is_following = YES;
        [self changeButtonToTick];
    }

}

- (void)changeStateOfFollowButton
{
    Company *company = [self getCompanyDataForViewController];
    if (company.is_following) {
        [self changeButtonToTick];
    }
    else
    {
        [self changeButtonToAdd];
    }
}

- (void)changeButtonToTick
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.followButton.image = [UIImage imageNamed:tick];
    });
}

- (void)changeButtonToAdd
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.followButton.image = [UIImage imageNamed:add];
    });
}

@end
