//
//  SwitchSlideView.h
//  projectSnowball
//
//  Created by Donald Fung on 1/17/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//
//  Credit to: B.H. Liu on 12-5-14.
//  Copyright (c) 2012年 Appublisher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwitchSlideView : UIView <UIScrollViewDelegate>

@property (nonatomic,retain) UIScrollView *scrollView;
//@property (nonatomic,retain) UILabel *titleLabel;
@property (nonatomic,retain) UIPageControl *pageControl;
@property (nonatomic,readwrite) NSInteger currentIndex;
@property (nonatomic,readwrite) NSInteger totalPages;

- (void)setObjectsWithArray:(NSArray*)array;

@end
