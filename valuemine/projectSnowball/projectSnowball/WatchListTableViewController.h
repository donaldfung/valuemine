//
//  WatchListTableViewController.h
//  projectSnowball
//
//  Created by Donald Fung on 1/15/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "BaseTableViewController.h"

@interface WatchListTableViewController : BaseTableViewController

@property (nonatomic, strong)NSMutableArray *savedCompanies;
@property (nonatomic, assign)int segmentedIndex;
@end
