//
//  SlideTwoViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 1/17/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "SlideTwoViewController.h"
#import "Company.h"
#import "UIColor+customColors.h"

@interface SlideTwoViewController ()

@property (weak, nonatomic) IBOutlet UIView *contentHolder;
@property (weak, nonatomic) IBOutlet UILabel *bookValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *bookValuePerShareLabel;
@property (weak, nonatomic) IBOutlet UILabel *ncavLabel;
@property (weak, nonatomic) IBOutlet UILabel *bookValueFigure;
@property (weak, nonatomic) IBOutlet UILabel *bookValuePerShareFigure;
@property (weak, nonatomic) IBOutlet UILabel *ncavFigure;
@end

@implementation SlideTwoViewController


#pragma mark View Lifecycle
- (void)viewDidLoad
{
    self.companyManager = [CompanyManager sharedCompany];
    [self setValuesForLabels];
    [self setBackGroundColor];
    [self configureLabels];
}

#pragma mark Configure UI

- (void)setBackGroundColor
{
    self.contentHolder.backgroundColor = [UIColor darkBlue];
}

- (void)configureLabels
{
    [self customizeLabelForSummaryView:self.bookValueFigure];
    [self customizeLabelForSummaryView:self.bookValueLabel];
    [self customizeLabelForSummaryView:self.ncavFigure];
    [self customizeLabelForSummaryView:self.ncavLabel];
    [self customizeLabelForSummaryView:self.bookValuePerShareFigure];
    [self customizeLabelForSummaryView:self.bookValuePerShareLabel];
}

#pragma mark Data for view
- (void)setValuesForLabels
{
    Company *company = self.companyManager.company;
    
    //  Get strings from numbers for view
    NSString *bookValue = [self formatNumberCurrencyStyle:company.bookValue];
    NSString *ncav = [self formatNumberCurrencyStyle:company.netCurrentAssetValuePerShare];
    NSString *bookValuePerShare = [self formatNumberCurrencyStyle:company.bookValuePerShare];
    
    //  Assign values to labels for view
    self.bookValueLabel.text = @"Book Value";
    self.bookValuePerShareLabel.text = @"Book Value/Share";
    self.ncavLabel.text = @"NCAV";
    self.bookValueFigure.text = bookValue;
    self.bookValuePerShareFigure.text = bookValuePerShare;
    self.ncavFigure.text = ncav;
}
@end
