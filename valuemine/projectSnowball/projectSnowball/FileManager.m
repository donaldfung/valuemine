//
//  FileManager.m
//  projectSnowball
//
//  Created by Donald Fung on 2/15/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "FileManager.h"
#import "Constants.h"

@implementation FileManager

+ (id)sharedFileManager
{
    static FileManager *sharedFileManager = nil;
    @synchronized(self)
    {
        if (sharedFileManager == nil)
        {
            sharedFileManager = [[self alloc]init];
        }
    }
    return sharedFileManager;
}

- (NSMutableDictionary *)saveQuandlList
{
    NSString *dbPath;
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docPath = [path objectAtIndex:0];
    dbPath = [docPath stringByAppendingPathComponent:quandlList];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success;
    success = [fileManager fileExistsAtPath:dbPath];
    if(!success){
        NSString *filePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:quandlList];
        [fileManager copyItemAtPath:filePathFromApp toPath:dbPath  error:nil];
    }
    NSString *readFile = [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:dbPath] encoding:NSUTF8StringEncoding];
    
    NSArray *fileComponents = [readFile componentsSeparatedByString:@"\n"];
    NSMutableDictionary * savedTickers = [[NSMutableDictionary alloc]init];
    for (int i = 0; i < fileComponents.count; i ++) {
        NSString *component = [fileComponents objectAtIndex:i];
        NSArray *splitComponent = [component componentsSeparatedByString:@"/"];
        NSString *getSecondElement = [splitComponent objectAtIndex:1];
        NSArray *splitThisElement = [getSecondElement componentsSeparatedByString:@","];
        NSString *ticker = [splitThisElement objectAtIndex:0];
        [savedTickers setValue:ticker forKey:ticker];
    }
    return savedTickers;
}

- (void)saveToDocumentsDirectory:(NSString *)fileName
{
    NSArray *searchPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docPath = [searchPath objectAtIndex:0];
    NSString *pathString = [docPath stringByAppendingPathComponent:fileName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL success;
    success = [fileManager fileExistsAtPath:pathString];
    if(!success){
        NSString *filePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:fileName];
        [fileManager copyItemAtPath:filePathFromApp toPath:pathString  error:nil];
    }
}

- (NSString *)documentPath:(NSString *)fileName
{
    NSArray *searchPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docPath = [searchPath objectAtIndex:0];
    NSString *pathString = [docPath stringByAppendingPathComponent:fileName];
    return pathString;
}

@end
