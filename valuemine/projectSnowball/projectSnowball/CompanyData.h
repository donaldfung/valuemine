//
//  CompanyData.h
//  
//
//  Created by Donald Fung on 12/5/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface CompanyData : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@property (nullable, nonatomic, retain) NSNumber *avgEarningsPerShare;
@property (nullable, nonatomic, retain) NSNumber *avgPriceToEarningsRatio;
@property (nullable, nonatomic, retain) NSNumber *bookValue;
@property (nullable, nonatomic, retain) NSNumber *bookValuePerShare;
@property (nullable, nonatomic, retain) NSNumber *cashAndCashEquivalents;
@property (nullable, nonatomic, retain) NSNumber *currentAssets;
@property (nullable, nonatomic, retain) NSNumber *currentLiabilities;
@property (nullable, nonatomic, retain) NSNumber *currentRatio;
@property (nullable, nonatomic, retain) NSNumber *debtToCapitalRatio;
@property (nullable, nonatomic, retain) NSNumber *dividends;
@property (nullable, nonatomic, retain) NSNumber *dividendsPerShare;
@property (nullable, nonatomic, retain) NSNumber *earningsPerShare;
@property (nullable, nonatomic, retain) NSNumber *fiscalYearEndPrice;
@property (nullable, nonatomic, retain) NSString *fiscalYearEndDate;
@property (nullable, nonatomic, retain) NSNumber *goodwill;
@property (nullable, nonatomic, retain) NSString *industry;
@property (nullable, nonatomic, retain) NSNumber *intangibleAssets;
@property (nullable, nonatomic, retain) NSNumber *inventory;
@property (nullable, nonatomic, retain) NSString *logo;
@property (nullable, nonatomic, retain) NSNumber *longTermDebt;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *netCurrentAssetValuePerShare;
@property (nullable, nonatomic, retain) NSNumber *netIncome;
@property (nullable, nonatomic, retain) NSNumber *otherLongTermLiabilities;
@property (nullable, nonatomic, retain) NSNumber *pegRatio;
@property (nullable, nonatomic, retain) NSNumber *priceToBookRatio;
@property (nullable, nonatomic, retain) NSNumber *priceToEarningsRatio;
@property (nullable, nonatomic, retain) NSNumber *quickRatio;
@property (nullable, nonatomic, retain) NSNumber *retainedEarnings;
@property (nullable, nonatomic, retain) NSNumber *revenues;
@property (nullable, nonatomic, retain) NSNumber *shareholdersEquity;
@property (nullable, nonatomic, retain) NSNumber *sharesOutstanding;
@property (nullable, nonatomic, retain) NSNumber *shortTermDebt;
@property (nullable, nonatomic, retain) NSNumber *stockPrice;
@property (nullable, nonatomic, strong) NSDictionary *stockPriceHistory;
@property (nullable, nonatomic, retain) NSString *ticker;
@property (nullable, nonatomic, retain) NSNumber *totalAssets;
@property (nullable, nonatomic, retain) NSNumber *totalExpenses;
@property (nullable, nonatomic, retain) NSNumber *totalLiabilities;
@property (nullable, nonatomic, retain) NSDictionary *tenYearData;
@property (nullable, nonatomic, retain) NSNumber *is_following;

@end


