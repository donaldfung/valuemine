//
//  TenYearTableViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 1/23/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "TenYearTableViewController.h"
#import "UIColor+customColors.h"
#import "UIFont+customFonts.h"
#import "Constants.h"
#import "TenYearDetailTableViewController.h"
#import "Flurry.h"

static NSString *tableviewHolderIdentifier = @"companyTableHeaderVC";
static NSString *tenYearListCellIdentifier =@"tenYearListCell";

@interface TenYearTableViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation TenYearTableViewController

#pragma mark View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark Configure UI

- (NSArray *)defineRowLabels
{
    NSArray *rowLabels = [[NSArray alloc]initWithObjects:cash_label, currentAssets_label,dividends_label,goodwill_label, netIncome_label, longtermLiab_label, retainedEarnings_label, revenue_label, shareholderEquity_label, sharesoutstanding_label, totalAssets_label, totalDebt_label, totalLiab_label, nil];
    
    return rowLabels;
}

#pragma mark Tableview Datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *rowLabels = [self defineRowLabels];
    return [rowLabels count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tenYearListCellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:tenYearListCellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    NSArray *rowLabels =  [self defineRowLabels];
    cell.textLabel.text = [rowLabels objectAtIndex:indexPath.row];
    cell.textLabel.font = [self setFontToHelveticaWithSizeFourteen:cell];
    cell.textLabel.textColor = [UIColor defaultTheme];
    return cell;
}

- (NSArray *)prepareDataForKey:(NSString *)key
{
    Company *company = [self getCompanyDataForTableView];
    NSDictionary *tenYearData = company.tenYearData;
    NSArray *figures = [tenYearData valueForKey:key];
    if (figures.count == 0) {
        NSNumber *zero = [NSNumber numberWithDouble:0];
        NSString *noDate = @"-";
       NSMutableArray *noData = [[NSMutableArray alloc]initWithObjects:noDate,zero, nil];
        NSArray *object = [[NSArray alloc]initWithObjects:noData, nil];
        return object;
    }
    return figures;
}

- (NSString *)getKeyForRowLabel:(NSString *)rowLabel
{
    NSMutableDictionary *data = [[NSMutableDictionary alloc]init];
    [data setValue:cash_key forKey:cash_label];
    [data setValue:currentAssets_key forKey:currentAssets_label];
    [data setValue:dividends_key forKey:dividends_label];
    [data setValue:intangibles_key forKey:goodwill_label];
    [data setValue:netIncome_key forKey:netIncome_label];
    [data setValue:longtermliabilities_key forKey:longtermLiab_label];
    [data setValue:retainedEarnings_key forKey:retainedEarnings_label];
    [data setValue:revenue_key forKey:revenue_label];
    [data setValue:equity_key forKey:shareholderEquity_label];
    [data setValue:sharesOutstanding_key forKey:sharesoutstanding_label];
    [data setValue:totalAssets_key forKey:totalAssets_label];
    [data setValue:debt_key forKey:totalDebt_label];
    [data setValue:totalLiabilities_key forKey:totalLiab_label];
    
    NSString *key = [data valueForKey:rowLabel];
    return key;
}

- (void)getDataForTable:(NSIndexPath *)indexPath
{
    NSArray *rowLabels =  [self defineRowLabels];
    self.figuresCategory = [rowLabels objectAtIndex:indexPath.row];
    NSString *key = [self getKeyForRowLabel:self.figuresCategory];
    self.figures = [self prepareDataForKey:key];
}

#pragma mark Tableview Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //  Once a cell's selected, let the delegate perform the segue push.
    if ([self.baseTableDelegate respondsToSelector:@selector(cellPressed)]) {
        [self.baseTableDelegate cellPressed];
    }
    
    [Flurry logEvent:@"Ten_Year_Data_Viewed"
      withParameters:@{@"Index Path":[NSString stringWithFormat:@"%@", indexPath]}];
}

#pragma mark Tableview delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CompanyDetail_Cell_Height;
}

@end
