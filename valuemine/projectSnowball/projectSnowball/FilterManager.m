//
//  FilterManager.m
//  projectSnowball
//
//  Created by Donald Fung on 1/16/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "FilterManager.h"
#import "CoreDataManager.h"
#import "FileManager.h"
#import "Constants.h"

@interface FilterManager ()

@end
@implementation FilterManager

+ (id)sharedFilter
{
    static FilterManager *sharedFilter = nil;
    @synchronized(self)
    {
        if (sharedFilter == nil)
        {
            sharedFilter = [[self alloc]init];
        }
    }
    return sharedFilter;
}

-(void)preloadData
{
    //  Store files in documents directory
    [[FileManager sharedFileManager] saveToDocumentsDirectory:nyse];
    [[FileManager sharedFileManager] saveToDocumentsDirectory:nasdaq];
    NSString *nyseFilePath = [[FileManager sharedFileManager] documentPath:nyse];
    NSString *nasdaqFilePath = [[FileManager sharedFileManager] documentPath:nasdaq];
    NSString *readNYSE = [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:nyseFilePath] encoding:NSUTF8StringEncoding];
    NSString *readNASDAQ = [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:nasdaqFilePath] encoding:NSUTF8StringEncoding];
    NSString *stringOfCompanies = [readNYSE stringByAppendingString:readNASDAQ];
    _dictionaryOfCompanies = [[NSMutableDictionary alloc]init];
    _tickerAndCompanies  = [[NSMutableArray alloc]init];
    _tickerKeys = [[NSMutableDictionary alloc]init];

    NSArray *companyArrays = [stringOfCompanies componentsSeparatedByString:@"\n"];
    NSDictionary *savedTickers = [[FileManager sharedFileManager] saveQuandlList];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        for (int i = 0; i < [companyArrays count] - 1; i ++)
        {
            [self storeData:companyArrays wDict:savedTickers atIndex:i];
        }
    });
}

- (void)storeData:(NSArray *)companyList wDict:(NSDictionary *)tickersDict atIndex:(int)index
{
    NSString *readObject = [companyList objectAtIndex:index];
    NSArray *splitObject = [readObject componentsSeparatedByString:@"\t"];
    NSString *ticker = [splitObject objectAtIndex:0];
    NSString *company = [splitObject objectAtIndex:1];
    NSString *retrievedTicker = [tickersDict valueForKey:ticker];
    
    if (retrievedTicker != nil) {
        [self.tickerKeys setValue:company forKey:ticker];
        [self.tickerAndCompanies addObject:ticker];
        [self.tickerAndCompanies addObject:company];
        NSMutableArray *tempTickers = [[NSMutableArray alloc]initWithObjects:ticker, nil];
        if ([self.dictionaryOfCompanies objectForKey:company])
        {
            tempTickers = [self.dictionaryOfCompanies objectForKey:company];
            [self.dictionaryOfCompanies removeObjectForKey:company];
            [tempTickers addObject:ticker];
            [self.dictionaryOfCompanies setValue:tempTickers forKey:company];
        }
        else
        {
            [self.dictionaryOfCompanies setValue:tempTickers forKey:company];
        }
    }
}
@end
