//
//  SearchViewController+BasicFilter.h
//  projectSnowball
//
//  Created by Donald Fung on 2/13/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController (BasicFilter)

- (void)basicFilter:(NSString *)input;
@end
