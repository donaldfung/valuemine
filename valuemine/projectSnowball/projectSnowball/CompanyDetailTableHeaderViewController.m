//
//  TableHeaderViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 1/23/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "CompanyDetailTableHeaderViewController.h"
#import "SummaryTableViewController.h"
#import "UIColor+customColors.h"
#import "Constants.h"
#import "CompanyDetailViewController.h"
#import "DVSwitch.h"
#import "UIFont+customFonts.h"


#define summaryVcID @"summaryTvCIdentifier"
#define companyDetailVc @"CompanyDetailVC"

@interface CompanyDetailTableHeaderViewController ()

@property (weak, nonatomic) IBOutlet UIView *summaryHolder;
@property (weak, nonatomic) IBOutlet UIView *segmentedControlHolder;
@property (strong, nonatomic) SummaryTableViewController  *summaryTableview;;
@property (nonatomic, strong) CompanyDetailViewController *companyDetailViewController;
@property (weak, nonatomic) IBOutlet UIView *topViewHolder;
@property (weak, nonatomic) IBOutlet UILabel *tickerLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceChangeLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomViewHolder;
@property (nonatomic, strong) DVSwitch *switcher;
@property (nonatomic, strong) Company *company;
@property (weak, nonatomic) IBOutlet UIView *backdrop;

@end

@implementation CompanyDetailTableHeaderViewController

#pragma mark View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //  get company data
    self.company = [self getCompanyDataForViewController];
    
    //  add subviews
    [self addSummaryView];
    [self addSegmentedControlHolder];
    [self addTopViewHolder];
}

#pragma mark Configure UI

- (void)addTopViewHolder
{
    self.topViewHolder.backgroundColor = [UIColor darkBlue];
    self.tickerLabel.text = self.company.ticker;
    self.tickerLabel.font = [UIFont fontWithName:helvetica_neue size:size_twentyfour];
    self.nameLabel.text = self.company.name;
    self.nameLabel.font = [UIFont fontWithName:helvetica_neue size:size_sixteen];
    NSNumber *num = self.company.stockPrice;
    NSString *stockPrice = [self formatNumberNoStyle:num];
    self.priceLabel.text = stockPrice;
    self.priceLabel.font = [UIFont fontWithName:helvetica_neue size:size_twentyfour];
    self.priceChangeLabel.text = [self priceChange:self.company];
}

//  Calculates the current price and percentage change over the previous day's close

- (NSString *)priceChange:(Company *)company
{
    //  Calculate dollar value difference between current and previous closing prices
    NSDictionary *priceHistory = company.stockPriceHistory;
    //currentPrice = priceHistory
    return @"";
}

- (void)addSummaryView
{
    self.backdrop.backgroundColor = [UIColor darkBlue];
    
    CGRect summaryHolderFrameSize = CGRectMake(0, 0, self.summaryHolder.frame.size.width, self.summaryHolder.frame.size.height);
    self.summaryTableview = [self.storyboard instantiateViewControllerWithIdentifier:summaryVcID];
    self.summaryTableview.tableView.scrollEnabled = NO;
    self.summaryTableview.view.frame = summaryHolderFrameSize;
    [self addChildViewController:self.summaryTableview];
    [self.summaryHolder addSubview:self.summaryTableview.view];
    [self.summaryTableview didMoveToParentViewController:self];
}

- (void)addSegmentedControlHolder
{
    self.bottomViewHolder.backgroundColor = [UIColor darkBlue];
    self.companyDetailViewController = [[CompanyDetailViewController alloc]init];
    self.segmentedControlHolder.backgroundColor = [UIColor darkBlue];
    self.switcher = [[DVSwitch alloc] initWithStringsArray:@[@"1 Year Summary", @"10 Year Financials"]];
    self.switcher.frame = CGRectMake(0, 0, self.segmentedControlHolder.frame.size.width, self.segmentedControlHolder.frame.size.height);
    self.switcher.backgroundColor = [UIColor segmentedControlBackground];
    self.switcher.font = [UIFont fontWithName:helvetica_neue_bold size:size_thirteen];
    self.switcher.labelTextColorInsideSlider = [UIColor darkBlue];
    self.switcher.labelTextColorOutsideSlider = [UIColor whiteColor];
    self.switcher.cornerRadius = size_twelve;
    self.switcher.sliderOffset = size_zero;
    [self.segmentedControlHolder addSubview:self.switcher];
    [self.switcher setPressedHandler:^(NSUInteger index) {
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:index]forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName:indexChanged object:nil userInfo:userInfo];
    }];
}



@end
