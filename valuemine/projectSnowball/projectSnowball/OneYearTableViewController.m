//
//  OneYearTableViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 1/20/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "OneYearTableViewController.h"
#import "OneYearTvCell.h"
#import "Constants.h"
#import "UIColor+customColors.h"


static NSString *balanceSheetStatment = @"Balance Sheet";
static NSString *incomeStatement = @"Income Statement";
static NSString *cashflowsStatement = @"Cash Flows";
static NSString *cellIdentifier = @"oneYearCellIdentifier";
static NSString *dataForSection = @"dataForSection";
static NSString *fiscalYear = @"Fiscal Year";
static NSString *tableviewHolderIdentifier = @"companyTableHeaderVC";

@interface OneYearTableViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) OneYearTvCell *oneYearCell;
@property (nonatomic, strong) NSMutableArray *headerTitles;
@property (nonatomic, strong) NSMutableArray *rowLabels;

@end

@implementation OneYearTableViewController

#pragma mark View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self labelsForContentInTableView];
}

#pragma mark Configure UI

- (void)labelsForContentInTableView
{
    //  Header Titles
    self.headerTitles = [[NSMutableArray alloc]initWithObjects:@"Fiscal Year", @"Income Statment", @"Balance Sheet", @"Cash Flows", nil];
    
    //  Row Labels
    NSArray *fiscalYearSection = [[NSArray alloc]initWithObjects:@"Year", nil];
    NSArray *incomeStatment = [[NSArray alloc]initWithObjects:@"Revenue",@"Net Income", nil];
    NSArray *balanceSheet = [[NSArray alloc]initWithObjects:@"Cash & Cash Equivalents",@"Total Current Assets", @"Intangible Assets", @"Total Assets", @"Total Debt", @"Other Long Term Liabilities", @"Total Liabilities", @"Shares Outstanding (Diluted)", @"Retained Earnings", @"Shareholder Equity", nil];
    NSArray *cashflows = [[NSArray alloc]initWithObjects:@"Dividends", nil];
    
    NSDictionary *fiscalYearSectionDict = @{dataForSection : fiscalYearSection};
    NSDictionary *incomeStatementDict = @{dataForSection : incomeStatment};
    NSDictionary *balanceSheetDict = @{dataForSection : balanceSheet};
    NSDictionary *cashFlowDict = @{dataForSection : cashflows};
    self.rowLabels = [[NSMutableArray alloc]init];
    [self.rowLabels addObject:fiscalYearSectionDict];
    [self.rowLabels addObject:incomeStatementDict];
    [self.rowLabels addObject:balanceSheetDict];
    [self.rowLabels addObject:cashFlowDict];
}

#pragma mark Tableview datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.headerTitles count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *rowLabelsDict = self.rowLabels[section];
    NSArray *sectionData = [rowLabelsDict valueForKey:dataForSection];
    return [sectionData count];
}

#pragma mark Tableview delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CompanyDetail_Cell_Height;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    self.oneYearCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!self.oneYearCell)
    {
        self.oneYearCell = [tableView dequeueReusableCellWithIdentifier: cellIdentifier];
    }
    
    //  no accessory type
    self.oneYearCell.accessoryType = UITableViewCellAccessoryNone;
    
    //  selection style - none
    self.oneYearCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    //  Populate Cells
    [self.oneYearCell dataForCellAtIndexPath:indexPath withArrayOfRowLabels:self.rowLabels];
    
    return self.oneYearCell;
}

#pragma mark Section Header Setup

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView;
    
    if (section == 0)
    {
        //  create new view
        sectionHeaderView = [self newSection:tableView];
        
        //  set size and settings for left label for header view
        UILabel *headerLeftLabel = [self leftLabel:sectionHeaderView];
        [sectionHeaderView addSubview:headerLeftLabel];
        [self getHeaderTitleForSection:section withLeftLabel:headerLeftLabel];
        
        //  create new right label
        UILabel *rightLabel = [self rightLabel:sectionHeaderView];
        [sectionHeaderView addSubview:rightLabel];
        [self setTextForRightLabel:rightLabel withSection:section];
    }
    
    else
    {
        //  create new view
        sectionHeaderView = [self newSection:tableView];
        
        //  set size and settings for left label for header view
        UILabel *headerLeftLabel = [self leftLabel:sectionHeaderView];
        [sectionHeaderView addSubview:headerLeftLabel];
        
        [self getHeaderTitleForSection:section withLeftLabel:headerLeftLabel];
    }
    
    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return CompanyDetail_Cell_Height;
}

- (UIView *)newSection:(UITableView *)tableview
{
    UIView *newView = [[UIView alloc] initWithFrame:
                       CGRectMake(0, 0, tableview.frame.size.width, 50)];
    newView.backgroundColor = [UIColor lightBlue];
    return newView;
}

- (UILabel *)leftLabel:(UIView *)sectionHeader
{
    CGFloat labelSize = size_sixteen;
    CGFloat y = (CompanyDetail_Cell_Height - labelSize)/2;
    CGFloat height = (sectionHeader.frame.size.height/2) - y + 3;
    UILabel *newLabel = [[UILabel alloc] initWithFrame:
                         CGRectMake(14, y, sectionHeader.frame.size.width * .7, height)];
    newLabel.backgroundColor = [UIColor clearColor];
    newLabel.textAlignment = NSTextAlignmentLeft;
    [newLabel setFont:[UIFont fontWithName:helvetica_neue size:size_sixteen]];
    return newLabel;
}

- (UILabel *)rightLabel:(UIView *)sectionHeader
{
    CGFloat labelSize = size_thirteen;
    CGFloat y = (CompanyDetail_Cell_Height - labelSize)/2;
    CGFloat height = (sectionHeader.frame.size.height/2) - y + 3;
    UILabel *newlabel = [[UILabel alloc] initWithFrame:
     CGRectMake((sectionHeader.frame.size.width)*.57, y, sectionHeader.frame.size.width *.4, height)];
    newlabel.backgroundColor = [UIColor clearColor];
    newlabel.textAlignment = NSTextAlignmentRight;
    [newlabel setFont:[UIFont fontWithName:helvetica_neue size:size_thirteen]];
    return newlabel;
}

- (void)getHeaderTitleForSection:(NSInteger)section withLeftLabel:(UILabel *)leftLabel
{
    leftLabel.textColor = [UIColor defaultTheme];
    NSString *headerTitle = @"";
    switch (section) {
        case 0:
            headerTitle = fiscalYear;
            leftLabel.text = headerTitle;
            break;
        case 1:
            headerTitle = incomeStatement;
            leftLabel.text = headerTitle;
            break;
        case 2:
            headerTitle = balanceSheetStatment;
            leftLabel.text = headerTitle;
            break;
        case 3:
            headerTitle = cashflowsStatement;
            leftLabel.text = headerTitle;
            break;
        default:
            break;
    }
}

- (void)setTextForRightLabel:(UILabel *)rightlabel withSection:(NSInteger)section
{
    rightlabel.textColor = [UIColor defaultTheme];
    NSString *headerTitle = @"";
    switch (section) {
        case 0:
            headerTitle = inMillions;
            rightlabel.text = headerTitle;
            break;
        case 1:
            break;
        case 2:
            break;
        case 3:
            break;
        default:
            break;
    }
}

@end
