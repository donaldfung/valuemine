//
//  SearchTableViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 1/16/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "SearchTableViewController.h"
#import "SearchViewControllerCell.h"
#import "Company.h"
#import "Constants.h"

static NSString *searchCellIdentifier = @"searchCellIdentifier";

@interface SearchTableViewController () <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

@property (nonatomic, strong) SearchViewControllerCell *searchTvCell;

@end
@implementation SearchTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.results count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.searchTvCell = [tableView dequeueReusableCellWithIdentifier:searchCellIdentifier forIndexPath:indexPath];
    
    if (self.searchTvCell == nil) {
        self.searchTvCell = [tableView dequeueReusableCellWithIdentifier:searchCellIdentifier];
    }
    self.searchTvCell.accessoryType = UITableViewCellAccessoryNone;
   
    Company *co = [self.results objectAtIndex:indexPath.row];
    self.searchTvCell.companyTicker.font = [UIFont setFontToHelveticaWithSizeFourteen];
    self.searchTvCell.companyTicker.text = co.ticker;
    
    self.searchTvCell.companyName.font = [self setFontToHelveticaWithSizeEleven:self.searchTvCell];
    self.searchTvCell.companyName.text = co.name;

    return self.searchTvCell;
}

#pragma mark Tableview Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Company *company = [self.results objectAtIndex:indexPath.row];
    
    if ([self.baseTableDelegate respondsToSelector:@selector(cellPressedWithCompany:)]) {
        [self.baseTableDelegate cellPressedWithCompany:company];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return SearchCellHeight;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if ([self.baseTableDelegate respondsToSelector:@selector(dismissKeyboard)]) {
        [self.baseTableDelegate dismissKeyboard];
    }
}


@end
