//
//  MainCustomTableViewCell.m
//  projectSnowball
//
//  Created by Blayne Chong on 2015-11-19.
//  Copyright © 2015 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "MainCustomTableViewCell.h"

@implementation MainCustomTableViewCell

- (void)awakeFromNib {
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
