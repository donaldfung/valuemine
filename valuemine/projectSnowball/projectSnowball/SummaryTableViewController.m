//
//  SummaryTableViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 1/17/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "SummaryTableViewController.h"
#import "SwitchSlideManager.h"

@interface SummaryTableViewController () <UITableViewDataSource, UITableViewDelegate>
{
    CGRect screenRectangle;
}
@property (nonatomic, retain) SwitchSlideManager *switchSlide;

@end

@implementation SummaryTableViewController

#pragma mark View LifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupSwitchSlide];
}

#pragma mark SwitchSlideView Setup

- (void)setupSwitchSlide
{
    screenRectangle = [[UIScreen mainScreen] bounds];
    NSArray *slideViewControllers = [NSArray arrayWithObjects:slideOneID,slideTwoID,nil];
    self.switchSlide = [[SwitchSlideManager alloc]initWithCustomFrame:CGRectMake(0, 0, self.view.frame.size.width, contentHeight)];
    [self.switchSlide configureViewWithObjects:slideViewControllers];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [cell addSubview:self.switchSlide];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return contentHeight;
}

@end
