//
//  ServerClient.m
//  projectSnowball
//
//  Created by Donald Fung on 12/27/15.
//  Copyright © 2015 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "ServerClient.h"
#import <AFNetworking/AFNetworking.h>
#import "CompanyManager.h"
#import "Constants.h"

NSString* const parseClass = @"Companies";
NSString* const ticker_parse = @"ticker";
NSString* const name_parse = @"name";
NSString* const company_parse = @"company_data";
NSString* const current_price = @"current_price";

@interface ServerClient ()
@property (nonatomic, strong) NSMutableArray *localObject;
@property (nonatomic, strong) Constants *constants;
@property (nonatomic, assign) BOOL companyExists;
@end
@implementation ServerClient

+ (ServerClient *)sharedClient {
    static ServerClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[self alloc] init];
    });
    
    return _sharedClient;
}

#pragma mark - User Side

- (void)fetchCompanyWithName:(NSString *)companyName andTicker:(NSString *)stockTicker
{
    NSString *ticker = stockTicker;
    NSMutableString *formattedTicker = [ticker mutableCopy];
    [formattedTicker replaceOccurrencesOfString:@"-"
                                     withString:@""
                                        options:0
                                          range:NSMakeRange(0, formattedTicker.length)];
    
    [formattedTicker replaceOccurrencesOfString:@"."
                                     withString:@""
                                        options:0
                                          range:NSMakeRange(0, formattedTicker.length)];
    
    //  Handle request here
    PFQuery *query = [PFQuery queryWithClassName:parseClass];
    [query whereKey:ticker_parse equalTo:formattedTicker];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %lu object.", (unsigned long)objects.count);
            if (objects.count > 0) {
                for (PFObject *object in objects) {
                    PFObject *newCompany = object;
                    NSDictionary *companyData = newCompany[company_parse];
                    NSDictionary *currentPriceData = newCompany[current_price];
                    NSMutableDictionary *companyInformation = [[NSMutableDictionary alloc]init];
                    [companyInformation setValue:companyData forKey:@"companyData"];
                    [companyInformation setValue:currentPriceData forKey:@"currentPrice"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[CompanyManager sharedCompany]newCompany:companyInformation];
                    });
                }
            }
        }
        else {
            // Log details of the failure
            //NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}


#pragma mark - Developer Side

- (void)checkIfCompanyExistsWithName:(NSString *)companyName andTicker:(NSString *)stockTicker
{
    //  Check Parse to see if the company exists
    PFQuery *query = [PFQuery queryWithClassName:parseClass];
    [query whereKey:ticker_parse equalTo:stockTicker];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            if (objects.count == 0) {
                self.companyExists = NO;
                [self getDataFromQuandl:companyName andTicker:stockTicker];
            }
            else {
                NSLog(@"Existing company about to be updated");
                self.companyExists = YES;
                [self getDataFromQuandl:companyName andTicker:stockTicker];
            }
        }
        else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}

- (void)getDataFromQuandl:(NSString *)companyName andTicker:(NSString *)stockTicker
{
    NSMutableDictionary * companyData = [[NSMutableDictionary alloc]init];
    self.constants = [[Constants alloc]init];
    
    NSOperationQueue *myQueue = [[NSOperationQueue alloc] init];
    [myQueue addOperationWithBlock:^{
        
        
        
        //  Format the ticker before sending it as part of the request.  Remove special characters (commas, periods, hyphens) if necessary
        NSString *ticker = stockTicker;
        NSMutableString *formattedTicker = [ticker mutableCopy];
        [formattedTicker replaceOccurrencesOfString:@"-"
                                         withString:@""
                                            options:0
                                              range:NSMakeRange(0, formattedTicker.length)];
        
        [formattedTicker replaceOccurrencesOfString:@"."
                                         withString:@""
                                            options:0
                                              range:NSMakeRange(0, formattedTicker.length)];
        
        
        
        //  add name and ticker properties for the company
        [companyData setValue:companyName forKey:@"companyName"];
        [companyData setValue:formattedTicker forKey:@"companyTicker"];
        
        //   an array of url strings that will be sent to quandl in a series of GET requests
        [self.constants arrayOfQuandlStrings];
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
        for (int i = 0; i < self.constants.urlArrray.count; i ++) {
            
            NSString *urlString = [self.constants.urlArrray objectAtIndex:i];
            NSString *quandlString = [NSString stringWithFormat:urlString, formattedTicker];
            NSURL *url = [NSURL URLWithString:quandlString];
            NSURLRequest *request = [NSURLRequest requestWithURL:url];
            NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
                
                if (error) {
                    NSLog(@"Error");
                }
                else
                {
                    // get data from Quandl request
                    NSMutableDictionary *dataset = [responseObject valueForKey:@"dataset"];
                    NSArray *data  = [dataset valueForKey:@"data"];
                    
                    // get sector from Market Cap data
                    
                    NSString *datasetCode = [dataset valueForKey:@"dataset_code"];
                    NSArray *splitCode = [datasetCode componentsSeparatedByString:@"_"];
                    NSString *key = [splitCode objectAtIndex:1];
                    if ([key isEqualToString:@"MARKETCAP"]) {
                        // do something
                        
                    }
                    
                    //  add data to dictionary & account for nil values
                    [companyData setValue:data forKey:key];
                    //  check to see if all the required data has been added to the dictionary before sending the dictionary to Company Detail VC
                    NSArray * allKeys = [companyData allKeys];
                    NSUInteger counter = [allKeys count];
                    if (counter == number_of_requests) {
                        if (self.companyExists == NO) {
                          //  NSDictionary *ratios = [[CompanyRatiosManager sharedRatiosManager]ratios:companyData];
                            
                            [self addCompanyToParseWithQuandlData:companyData andTicker:formattedTicker andRatios:nil andName:companyName];
                        }
                        else {
                           // NSDictionary *ratios = [[CompanyRatiosManager sharedRatiosManager]ratios:companyData];
                            [self updateCompanyOnParseWithQuandlData:companyData andTicker:formattedTicker andRatios:nil andName:companyName];
                        }
                        
                    }
                    
                }
            }];
            [dataTask resume];
        }
        
    }];
    
}

#pragma mark Add New Company
- (void)addCompanyToParseWithQuandlData:(NSDictionary *)quandlData andTicker:(NSString *)ticker andRatios:(NSDictionary *)ratios andName:(NSString *)companyName
{
    PFObject *newCompany = [PFObject objectWithClassName:parseClass];
    newCompany[ticker_parse] = ticker;
    newCompany[company_parse] = quandlData;
    newCompany[name_parse] = companyName;
    [newCompany saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            // The object has been saved.
            NSLog(@"%@ updated!",ticker);
        } else {
            // There was a problem, check error.description
            NSLog(@"%@",error);
        }
    }];
}

#pragma mark Update Existing Company
- (void)updateCompanyOnParseWithQuandlData:(NSDictionary *)quandlData andTicker:(NSString *)ticker andRatios:(NSDictionary *)ratios andName:(NSString *)companyName
{
    PFQuery *query = [PFQuery queryWithClassName:parseClass];
    [query whereKey:ticker_parse equalTo:ticker];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            if (objects.count == 1) {
                PFObject *existingCompany = [objects objectAtIndex:0];
                existingCompany[company_parse] = quandlData;
                existingCompany[name_parse] = companyName;
                NSLog(@"%@ updated!",ticker);
                [existingCompany saveInBackground];
            }
        }
        else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}

#pragma mark - Update Prices

- (void)updateCompanyWithTicker:(NSString *)ticker
{
    NSMutableString *formattedTicker = [ticker mutableCopy];
    [formattedTicker replaceOccurrencesOfString:@"-"
                                     withString:@""
                                        options:0
                                          range:NSMakeRange(0, formattedTicker.length)];
    
    [formattedTicker replaceOccurrencesOfString:@"."
                                     withString:@""
                                        options:0
                                          range:NSMakeRange(0, formattedTicker.length)];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSString *urlString = [NSString stringWithFormat:CurrentStockPrice, formattedTicker];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        if (error) {
            NSLog(@"Error");
        }
        else
        {
            // get data from Quandl request
            NSMutableDictionary *dataset = [responseObject valueForKey:@"dataset"];
            NSArray *data  = [dataset valueForKey:@"data"];
            NSArray *lastDailyPrices = [data objectAtIndex:0];
            NSString *date = [lastDailyPrices objectAtIndex:0];
            NSNumber *closePrice = [lastDailyPrices objectAtIndex:4];
            NSMutableDictionary *currentDic = [[NSMutableDictionary alloc]init];
            [currentDic setValue:date forKey:@"date"];
            [currentDic setValue:closePrice forKey:@"closePrice"];
            
            
            if (currentDic.count > 0) {
                [self updatePrices:currentDic usingTicker:formattedTicker];
            }
        }
    }];
    [dataTask resume];
}

- (void)updatePrices:(NSDictionary *)dictionary usingTicker:(NSString *)ticker
{
    PFQuery *query = [PFQuery queryWithClassName:parseClass];
    [query whereKey:ticker_parse equalTo:ticker];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            if (objects.count == 1) {
                PFObject *existingCompany = [objects objectAtIndex:0];
                existingCompany[current_price] = dictionary;
                NSLog(@"Company updated!");
                [existingCompany saveInBackground];
            }
        }
        else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}


@end
