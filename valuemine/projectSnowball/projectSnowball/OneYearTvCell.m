//
//  OneYearTvCell.m
//  projectSnowball
//
//  Created by Donald Fung on 1/22/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "OneYearTvCell.h"
#import "UIColor+customColors.h"

static NSString *dataForSection = @"dataForSection";

@interface OneYearTvCell ()
@property (weak, nonatomic) IBOutlet UILabel *figuresLabel;
@property (weak, nonatomic) IBOutlet UILabel *rowLabel;


@end
@implementation OneYearTvCell

- (void)dataForCellAtIndexPath:(NSIndexPath *)indexPath withArrayOfRowLabels:(NSArray *)rowLabelsArray
{
    NSInteger section = indexPath.section;
    //  Row Labels
    NSDictionary *data = rowLabelsArray[indexPath.section];
    NSArray *rowData =  [data valueForKey:dataForSection];
    NSString *rowLabel = rowData[indexPath.row];
    self.rowLabel.text = rowLabel;
    self.rowLabel.textColor = [UIColor defaultTheme];
    self.rowLabel.frame = CGRectMake(0, 0, 70, 40);
    
    //  Figures
    Company *company = [self getCompanyDataForCell];
    if (self.companyManager != nil)
    {
        NSArray *figuresForCells = [self prepareDataForSection:section withCompany:company];
        self.figuresLabel.text = figuresForCells[indexPath.row];
        self.figuresLabel.textColor = [UIColor defaultTheme];
    }
}

- (NSArray *)prepareDataForSection:(NSInteger)section withCompany:(Company *)company
{
    NSArray *data;
    switch (section) {
        case 0:
            data = [self didFinishPreparingFiscalYearEnded:company];
            break;
        case 1:
            data = [self didFinishPreparingIncomeStatementData:company];
            break;
        case 2:
            data = [self didFinishPreparingBalanceSheetData:company];
            break;
        case 3:
            data = [self didFinishPreparingCashFlowStatementData:company];
            break;
        default:
            break;
    }
    
    return data;
}

- (NSArray *)didFinishPreparingFiscalYearEnded:(Company *)company
{
    NSArray *fiscalYearEnded = [[NSArray alloc]initWithObjects:company.fiscalYearEndDate, nil];
    return fiscalYearEnded;
}

- (NSArray *)didFinishPreparingIncomeStatementData:(Company *)company
{
    NSString *revenues = [self formatNumberCurrencyStyle:company.revenues];
    NSString *netIncome =[self formatNumberCurrencyStyle:company.netIncome];
    NSMutableArray *figures = [[NSMutableArray alloc]init];
    [figures addObject:revenues];
    [figures addObject:netIncome];
    return figures;
}

- (NSArray *)didFinishPreparingBalanceSheetData:(Company *)company
{
    NSString *cash =[self formatNumberCurrencyStyle:company.cashAndCashEquivalents];
    NSString *currentAssets = [self formatNumberCurrencyStyle:company.currentAssets];
    NSString *intangibles = [self formatNumberCurrencyStyle:company.intangibleAssets];
    NSString *totalAssets = [self formatNumberCurrencyStyle:company.totalAssets];
    NSString *longTermDebt = [self formatNumberCurrencyStyle:company.longTermDebt];
    NSString *otherLongTermLiab = [self formatNumberCurrencyStyle:company.otherLongTermLiabilities];
    NSString *totalLiab = [self formatNumberCurrencyStyle:company.totalLiabilities];
    NSString *sharesOutstanding =[self stringFromNumberWithDecimalStyle:company.sharesOutstanding];
    NSString *retainedEarnings =[self formatNumberCurrencyStyle:company.retainedEarnings];
    NSString *equity =[self formatNumberCurrencyStyle:company.shareholdersEquity];
    
    NSMutableArray *figures = [[NSMutableArray alloc]init];
    [figures addObject:cash];
    [figures addObject:currentAssets];
    [figures addObject:intangibles];
    [figures addObject:totalAssets];
    [figures addObject:longTermDebt];
    [figures addObject:otherLongTermLiab];
    [figures addObject:totalLiab];
    [figures addObject:sharesOutstanding];
    [figures addObject:retainedEarnings];
    [figures addObject:equity];
    return figures;
}

- (NSArray *)didFinishPreparingCashFlowStatementData:(Company *)company
{
    NSString *divs =[self formatNumberCurrencyStyle:company.dividends];
    NSArray *figures = [[NSArray alloc]initWithObjects:divs, nil];
    return figures;
}

@end
