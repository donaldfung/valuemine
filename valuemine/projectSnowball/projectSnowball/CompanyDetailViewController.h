//
//  CompanyDetailViewController.h
//  projectSnowball
//
//  Created by Blayne Chong on 2015-11-10.
//  Copyright © 2015 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface CompanyDetailViewController : BaseViewController

@end
