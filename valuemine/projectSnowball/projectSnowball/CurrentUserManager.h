//
//  NewUsersClient.h
//  projectSnowball
//
//  Created by Donald Fung on 1/10/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface CurrentUserManager : NSObject

@property (nonatomic, strong) User *currentUser;

+ (id)sharedCurrentUserManager;

@end
