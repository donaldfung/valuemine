//
//  UIFont+customFonts.m
//  projectSnowball
//
//  Created by Donald Fung on 1/30/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "UIFont+customFonts.h"
#import "Constants.h"

@implementation UIFont (customFonts)


+ (UIFont *)setFontToHelveticaWithSizeFourteen
{
    UILabel *label;
    label.font = [UIFont fontWithName:helvetica_neue size:size_fourteen];
    return label.font;
}

+ (UIFont *)setFontToHelveticaWithSizeSixteen
{
    UILabel *label;
    label.font = [UIFont fontWithName:helvetica_neue size:size_sixteen];
    return label.font;
}


+ (UIFont *)setFontToHelveticaWithSizeTwentyFour
{
        UILabel *label;
        label.font = [UIFont fontWithName:helvetica_neue size:size_twentyfour];
        return label.font;
}
@end
