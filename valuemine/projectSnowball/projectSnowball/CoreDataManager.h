//
//  DAO.h
//  projectSnowball
//
//  Created by Donald Fung on 9/19/15.
//  Copyright © 2015 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "CompanyData.h"
#import "Company.h"

typedef void (^finishedWithSuccess)(BOOL);

@interface CoreDataManager : NSObject

+ (id)coreDataManager;
- (void)setupEntityDescription;
- (void)saveChanges;
- (void)saveCompany:(Company *)company;
- (void)deleteCompany:(Company *)company;
- (void)updateCompany:(Company *)company finished:(finishedWithSuccess)updated;
- (NSMutableArray *)fetchCompanies;
@end
