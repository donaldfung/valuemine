//
//  SearchViewController.h
//  projectSnowball
//
//  Created by Donald Fung on 10/16/15.
//  Copyright © 2015 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "SearchTableViewController.h"

static NSString *searchCellTapped = @"searchCellTapped";
static NSString *companyDataReadyForUse = @"companyData";

@interface SearchViewController : BaseViewController

@property (nonatomic, strong) SearchTableViewController *searchTVC;
@property (nonatomic, copy) NSArray *tickersAndCompaniesData;
@property (nonatomic, copy) NSDictionary *companyDictionaryData;
@property (nonatomic, strong) NSMutableArray *companiesForTableView;
@property (nonatomic, strong) NSDictionary *tickerKeyData;

@end
