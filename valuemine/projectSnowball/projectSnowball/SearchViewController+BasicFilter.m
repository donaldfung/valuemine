//
//  SearchViewController+BasicFilter.m
//  projectSnowball
//
//  Created by Donald Fung on 2/13/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "SearchViewController+BasicFilter.h"

@implementation SearchViewController (BasicFilter)

- (void)basicFilter:(NSString *)input
{
    __block NSArray *result;
    [self.companiesForTableView removeAllObjects];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
    {
        if (self.tickersAndCompaniesData != nil)
        {
            if (![input  isEqual: @""])
            {
                NSString *formattedInput = [input stringByAppendingString:@"*"];
                result = [self applyFilter:formattedInput];
            }
            
            if (result.count < 1)
            {
                [self.companiesForTableView removeAllObjects];
            }
            
            for (NSString *strings in result)
            {
                if (self.companyDictionaryData)
                {
                    NSString *nameCheck = [self.companyDictionaryData objectForKey:strings];
                    if (nameCheck == nil)
                    {
                        [self prepareDataUsingTicker:strings];
                    }
                    else
                    {
                        [self prepareDataUsingCompanyName:strings];
                    }
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self reloadTableview];
            });
        }
    });
}

#pragma mark Helper Methods

- (NSMutableArray *)applyFilter:(NSString *)input
{
    NSMutableArray *results = [[NSMutableArray alloc]init];
    if (input.length == 1)
    {
       results = [self applyPredicateWithFormat:@"SELF matches[c] %@" wResults:results andInput:input];
    }
    else
    {
        results = [self applyPredicateWithFormat:@"SELF like[c] %@" wResults:results andInput:input];
    }
    return results;
}

- (NSMutableArray *)applyPredicateWithFormat:(NSString *)format wResults:(NSMutableArray *)results andInput:(NSString *)input
{
    NSPredicate *wildCardPredicate = [NSPredicate predicateWithFormat:format, input];
    NSArray *filteredObject = [self.tickersAndCompaniesData filteredArrayUsingPredicate:wildCardPredicate];
    [results addObjectsFromArray:filteredObject];
    return results;
}

- (void)prepareDataUsingTicker:(NSString *)ticker
{
    if ([self.tickerKeyData objectForKey:ticker]) {
        NSMutableArray *arrayOfData = [[NSMutableArray alloc]init];
        [arrayOfData addObject:[self.tickerKeyData objectForKey:ticker]];
        Company *company = [[Company alloc]init];
        
        
        for (NSString *stringOfContent in arrayOfData)
        {
            company.name = stringOfContent;
            company.ticker = ticker;
            
            if (self.companiesForTableView.count < 1)
            {
                [self.companiesForTableView addObject:company];
            }
            else {
                NSString *checkTicker = company.ticker;
                BOOL absent = NO;
                for (Company *companies in self.companiesForTableView)
                {
                    
                    if (checkTicker != companies.ticker)
                    {
                        absent = YES;
                        
                    }
                    else {
                        //do nothing
                        absent = NO;
                    }
                    
                }
                if (absent == YES) {
                    [self.companiesForTableView addObject:company];
                }
                
            }
        }
    }
}

- (void)prepareDataUsingCompanyName:(NSString *)companyName
{
    if ([self.companyDictionaryData objectForKey:companyName])
    {
        NSMutableArray *arrayofData = [[NSMutableArray alloc]init];
        [arrayofData addObject:[self.companyDictionaryData objectForKey:companyName]];
        NSArray *getObjectsWithinArray = [arrayofData firstObject];
        Company *company = [[Company alloc]init];
        
        
        for (int i = 0; i < getObjectsWithinArray.count; i ++)
        {
            company.ticker = [getObjectsWithinArray objectAtIndex:i];
            company.name = companyName;
            
            if (self.companiesForTableView.count < 1)
            {
                [self.companiesForTableView addObject:company];
            }
            else {
                NSString *checkTicker = company.ticker;
                NSString *checkName = company.name;
                
                BOOL absent = NO;
                for (Company *companies in self.companiesForTableView)
                {
                    
                    if ((checkTicker != companies.ticker) && (checkName != companies.name))
                    {
                        absent = YES;
                        
                    }
                    else {
                        absent = NO;
                    }
                    
                }
                if (absent == YES) {
                    [self.companiesForTableView addObject:company];
                }
                
            }
        }
    }

}

- (void)reloadTableview
{
    self.searchTVC.results = [[NSArray alloc]init];
    self.searchTVC.results = self.companiesForTableView;
    [self.searchTVC.tableView reloadData];
}

@end
