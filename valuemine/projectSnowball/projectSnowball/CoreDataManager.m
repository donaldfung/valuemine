//
//  CoreDataManager.m
//  projectSnowball
//
//  Created by Donald Fung on 9/19/15.
//  Copyright © 2015 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "CoreDataManager.h"
#import "CompanyData.h"

#define EntityName @"CompanyData"

@interface CoreDataManager ()

@property (nonatomic, strong) AppDelegate *appDelegate;
@property (nonatomic, strong) NSEntityDescription *managedCompany;
@end
@implementation CoreDataManager

-(id)init
{
    if (self == [super init])
    {
        _appDelegate = [[AppDelegate alloc]init];
        return self;
    }
    else
    {
        return nil;
    }
}

+(id)coreDataManager
{
    static CoreDataManager *coreDataManager = nil;
    @synchronized(self)
    {
        if (coreDataManager == nil)
        {
            coreDataManager = [[self alloc]init];
        }
    }
    return coreDataManager;
}

- (void)setupEntityDescription
{
    self.managedCompany = [NSEntityDescription entityForName:EntityName inManagedObjectContext:self.appDelegate.managedObjectContext];
}

- (void)saveChanges
{
    NSError *err = nil;
    BOOL successful = [self.appDelegate.managedObjectContext save:&err];
    if(!successful){
        NSLog(@"Error saving: %@", [err localizedDescription]);
    }else {
        NSLog(@"Data Saved");
    }
}


#pragma mark - save/fetch company

- (void)saveCompany:(Company *)company
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:self.managedCompany];
    NSError *error = nil;
    NSArray *result = [self.appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(!result)
    {
        [NSException raise:@"Fetch Failed" format:@"Reason: %@", [error localizedDescription]];
    }
    else
    {
        [self createManagedObject:company];
    }
}

- (void)createManagedObject:(Company *)company
{
    NSManagedObject *object = [[NSManagedObject alloc]initWithEntity:self.managedCompany insertIntoManagedObjectContext:self.appDelegate.managedObjectContext];
    [self setValuesForManagedObject:object withCompany:company];
    [self saveChanges];
}

- (NSMutableArray *)fetchCompanies
{
    NSMutableArray *savedCompanies = [[NSMutableArray alloc]init];
    NSEntityDescription *entity = [[self.appDelegate.managedObjectModel entitiesByName] objectForKey:EntityName];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *result = [self.appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if(!result){
        [NSException raise:@"Fetch Failed" format:@"Reason: %@", [error localizedDescription]];
    }
    else if (result.count == 0)
    {
        
    }
    else
    {
        for (CompanyData *managedCompanyObjects in result)
        {
            Company *existingCompany = [[Company alloc]init];
            
            //  Give this new Company instance some properties
            [existingCompany setTicker:managedCompanyObjects.ticker];
            [existingCompany setName:managedCompanyObjects.name];
            [existingCompany setStockPrice:managedCompanyObjects.stockPrice];
            [existingCompany setStockPriceHistory:managedCompanyObjects.stockPriceHistory];
            [existingCompany setRevenues:managedCompanyObjects.revenues];
            [existingCompany setNetIncome:managedCompanyObjects.netIncome];
            [existingCompany setEarningsPerShare:managedCompanyObjects.earningsPerShare];
            [existingCompany setRetainedEarnings:managedCompanyObjects.retainedEarnings];
            [existingCompany setDividends:managedCompanyObjects.dividends];
            [existingCompany setBookValue:managedCompanyObjects.bookValue];
            [existingCompany setBookValuePerShare:managedCompanyObjects.bookValuePerShare];
            [existingCompany setSharesOutstanding:managedCompanyObjects.sharesOutstanding];
            [existingCompany setPriceToEarningsRatio:managedCompanyObjects.priceToEarningsRatio];
            [existingCompany setAvgEarningsPerShare:managedCompanyObjects.avgEarningsPerShare];
            [existingCompany setPriceToBookRatio:managedCompanyObjects.priceToBookRatio];
            [existingCompany setPegRatio:managedCompanyObjects.pegRatio];
            [existingCompany setNetCurrentAssetValuePerShare:managedCompanyObjects.netCurrentAssetValuePerShare];
            [existingCompany setCurrentRatio:managedCompanyObjects.currentRatio];
            [existingCompany setCashAndCashEquivalents:managedCompanyObjects.cashAndCashEquivalents];
            [existingCompany setInventory:managedCompanyObjects.inventory];
            [existingCompany setCurrentAssets:managedCompanyObjects.currentAssets];
            [existingCompany setGoodwill:managedCompanyObjects.goodwill];
            [existingCompany setIntangibleAssets:managedCompanyObjects.intangibleAssets];
            [existingCompany setTotalAssets:managedCompanyObjects.totalAssets];
            [existingCompany setCurrentLiabilities:managedCompanyObjects.currentLiabilities];
            [existingCompany setLongTermDebt:managedCompanyObjects.longTermDebt];
            [existingCompany setOtherLongTermLiabilities:managedCompanyObjects.otherLongTermLiabilities];
            [existingCompany setTotalLiabilities:managedCompanyObjects.totalLiabilities];
            [existingCompany setShareholdersEquity:managedCompanyObjects.shareholdersEquity];
            [existingCompany setDividendsPerShare:managedCompanyObjects.dividendsPerShare];
            [existingCompany setQuickRatio:managedCompanyObjects.quickRatio];
            [existingCompany setFiscalYearEndDate:managedCompanyObjects.fiscalYearEndDate];
            [existingCompany setFiscalYearEndPrice:managedCompanyObjects.fiscalYearEndPrice];
            [existingCompany setDebtToCapitalRatio:managedCompanyObjects.debtToCapitalRatio];
            [existingCompany setTenYearData:managedCompanyObjects.tenYearData];
            
            
            NSNumber *boolAsNumber = managedCompanyObjects.is_following;
            BOOL getBool = [boolAsNumber boolValue];
            [existingCompany setIs_following:getBool];
            [savedCompanies addObject:existingCompany];
        }
    }
    return savedCompanies;
}

-(void)deleteCompany:(Company *)company
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    NSEntityDescription *entity = [[self.appDelegate.managedObjectModel entitiesByName] objectForKey:EntityName];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    
    //  An array to store the companies retrieved
    NSArray *result = [self.appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (!result) {
        [NSException raise:@"Fetch Failed" format:@"Reason: %@", [error localizedDescription]];
    }
    else {
        //  If company shows up in retrieved results, then delete it
        for (CompanyData *existingCompany in result) {
            if ([company.ticker isEqualToString: existingCompany.ticker]) {
                
                //delete selected product
                
                [self.appDelegate.managedObjectContext deleteObject: existingCompany];
                NSLog(@"%@ deleted",existingCompany.name);
                
                //  save changes made to persistent store
                [self saveChanges];
                break;
            }
        }
    }
}

- (void)updateCompany:(Company *)company finished:(finishedWithSuccess)updated
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc]init];
    NSEntityDescription *entity = [[self.appDelegate.managedObjectModel entitiesByName] objectForKey:EntityName];
    [fetchRequest setEntity:entity];
    NSError *error = nil;
    NSArray *result = [self.appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (!result) {
        [NSException raise:@"Fetch Failed" format:@"Reason: %@", [error localizedDescription]];
    }
    else
    {
        for (CompanyData *existingCompany in result) {
            if ([company.ticker isEqualToString: existingCompany.ticker])
            {
                NSManagedObject *existingObject = existingCompany;
                [self setValuesForManagedObject:existingObject withCompany:company];
                NSLog(@"%@ updated",company.name);
                [self saveChanges];
            }
        }
    }
}

- (void)setValuesForManagedObject:(NSManagedObject *)object withCompany:(Company *)company
{
    [object setValue:company.name forKey:@"name"];
    [object setValue:company.ticker forKey:@"ticker"];
    [object setValue:company.stockPrice forKey:@"stockPrice"];
    [object setValue:company.stockPriceHistory forKey:@"stockPriceHistory"];
    [object setValue:company.revenues forKey:@"revenues"];
    [object setValue:company.netIncome forKey:@"netIncome"];
    [object setValue:company.earningsPerShare forKey:@"earningsPerShare"];
    [object setValue:company.retainedEarnings forKey:@"retainedEarnings"];
    [object setValue:company.dividends forKey:@"dividends"];
    [object setValue:company.bookValue forKey:@"bookValue"];
    [object setValue:company.bookValuePerShare forKey:@"bookValuePerShare"];
    [object setValue:company.sharesOutstanding forKey:@"sharesOutstanding"];
    [object setValue:company.priceToEarningsRatio forKey:@"priceToEarningsRatio"];
    [object setValue:company.avgEarningsPerShare forKey:@"avgEarningsPerShare"];
    [object setValue:company.priceToBookRatio forKey:@"priceToBookRatio"];
    [object setValue:company.pegRatio forKey:@"pegRatio"];
    [object setValue:company.netCurrentAssetValuePerShare forKey:@"netCurrentAssetValuePerShare"];
    [object setValue:company.currentRatio forKey:@"currentRatio"];
    [object setValue:company.cashAndCashEquivalents forKey:@"cashAndCashEquivalents"];
    [object setValue:company.inventory forKey:@"inventory"];
    [object setValue:company.currentAssets forKey:@"currentAssets"];
    [object setValue:company.goodwill forKey:@"goodwill"];
    [object setValue:company.intangibleAssets forKey:@"intangibleAssets"];
    [object setValue:company.totalAssets forKey:@"totalAssets"];
    [object setValue:company.currentLiabilities forKey:@"currentLiabilities"];
    [object setValue:company.longTermDebt forKey:@"longTermDebt"];
    [object setValue:company.otherLongTermLiabilities forKey:@"otherLongTermLiabilities"];
    [object setValue:company.totalLiabilities forKey:@"totalLiabilities"];
    [object setValue:company.shareholdersEquity forKey:@"shareholdersEquity"];
    [object setValue:company.dividendsPerShare forKey:@"dividendsPerShare"];
    [object setValue:company.quickRatio forKey:@"quickRatio"];
    [object setValue:company.fiscalYearEndDate forKey:@"fiscalYearEndDate"];
    [object setValue:company.fiscalYearEndPrice forKey:@"fiscalYearEndPrice"];
    [object setValue:company.debtToCapitalRatio forKey:@"debtToCapitalRatio"];
    [object setValue:company.tenYearData forKey:@"tenYearData"];
    
    int one = 1;
    NSNumber *following = [NSNumber numberWithInt:one];
    [object setValue:following forKey:@"is_following"];
}

@end
