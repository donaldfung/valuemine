//
//  SearchViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 10/16/15.
//  Copyright © 2015 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchViewControllerCell.h"
#import <AFNetworking/AFNetworking.h>
#import "CompanyDetailViewController.h"
#import "ServerClient.h"
#import "SearchViewController+BasicFilter.h"
#import "FilterManager.h"

#define SearchTitle @"Search"

@interface SearchViewController () <BaseTableDelegate, UISearchControllerDelegate, UISearchBarDelegate>
//@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;        // uncomment later
@property (strong, nonatomic) IBOutlet UIView *searchAndFilterHolder;
@property (strong, nonatomic) IBOutlet UIView *tableHolder;
@property (nonatomic, strong) UISearchBar *searchBar;

@end

@implementation SearchViewController

#pragma mark View Lifecycle


/*  TEMP CHANGES FOR SEARCH BAR.
    
 1.  commented 2 methods in viewDidLoad for nav bar title, filter button and previous search bar
 2.  add method to hide  view containing the old search bar and filter button
 3.  adjust height constraints for tableview holder in xib.  Top space was originally 53 points from top superview.  Now it is set to 0.
 4.  commented IBOUTLET for old search bar and created a new but temp search bar property
 5.  add method for handling the creation of search bar embedded in the nav bar
 6.  customized appearance of bar button

 */

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self setNavBarTitle:SearchTitle];   // uncomment later
    //[self setUpSearchAndFilter]; // uncomment later
    [self addTableViewAsSubview];
    [self tempNavBarSetup];
    [self configureNotifications];
    [self.searchBar becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.searchBar resignFirstResponder];
}

#pragma mark - Configure UI

- (void)tempNavBarSetup
{
    self.navigationItem.leftBarButtonItem.title = nil;
    self.navigationItem.leftBarButtonItem.enabled = NO;
    self.navigationItem.leftBarButtonItem.image = nil;
    self.searchAndFilterHolder.hidden = YES;
    [self addSearchBarToNavBar];
}

- (void)addSearchBarToNavBar
{
    UIView *searchBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, screenWidth *.75, 44.0)];
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(-5, 0, screenWidth * .75, 44)];
    UIBarButtonItem *searchBarItem = [[UIBarButtonItem alloc] initWithCustomView:searchBarView];
    self.searchBar.delegate = self;
    [searchBarView addSubview:self.searchBar];
    self.navigationItem.leftBarButtonItem = searchBarItem;
    [self setUpSearchAndFilter];
    
    UIBarButtonItem *rightButton =
    [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(didClickCancelButton)];
    [rightButton setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor whiteColor], NSForegroundColorAttributeName,nil]
                          forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightButton;

}

- (void)setUpSearchAndFilter
{
    FilterManager *filterManager = [FilterManager sharedFilter];
    self.tickersAndCompaniesData = [filterManager.tickerAndCompanies copy];
    self.companyDictionaryData = [filterManager.dictionaryOfCompanies copy];
    self.tickerKeyData = [filterManager.tickerKeys mutableCopy];
    self.companiesForTableView = [[NSMutableArray alloc]init];
    self.searchBar.searchBarStyle = UIBarStyleDefault;
    self.searchBar.barStyle = UIBarStyleDefault;
    self.searchBar.barTintColor = [UIColor darkBlue];
    [self.searchBar setTintColor:[UIColor grayColor]];
    
    //  remove some unwanted lines above and below search bar
    CGRect rect = self.searchBar.frame;
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, rect.size.height-2,rect.size.width, 2)];
    UIView *topLineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,rect.size.width, 7)];
    topLineView.backgroundColor = [UIColor darkBlue];
    lineView.backgroundColor = [UIColor darkBlue];
    [self.searchBar addSubview:lineView];
    [self.searchBar addSubview:topLineView];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self basicFilter:searchText];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didClickCancelButton
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark TableView 

- (void)addTableViewAsSubview
{
    //  Size of content
    CGRect rect = self.navigationController.navigationBar.frame;
    float y = rect.size.height;
    self.tableHolder.frame = CGRectMake(0, y, screenWidth, screenHeight);
    self.searchTVC = [self.storyboard instantiateViewControllerWithIdentifier:search_tvc_Identifier];
    self.searchTVC.baseTableDelegate = self;
    self.searchTVC.view.frame = CGRectMake(0.0, 0.0, screenWidth, screenHeight);
    [self addChildViewController:self.searchTVC];
    [self.tableHolder addSubview:self.searchTVC.view];
    [self.searchTVC didMoveToParentViewController:self];
}

#pragma mark search filter


- (void)queryResults:(NSArray *)results
{

}

#pragma mark Base Table Delegate

- (void)cellPressedWithCompany:(Company *)company
{
    [[CompanyManager sharedCompany]prepareRequest:company];
}

#pragma mark Buttons

- (IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)filterButton:(id)sender {
}

#pragma mark Keyboard

- (void)configureNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissKeyboard) name:UIKeyboardWillHideNotification object:nil];
}

- (void)dismissKeyboard
{
    [self.searchBar resignFirstResponder];
}
@end
