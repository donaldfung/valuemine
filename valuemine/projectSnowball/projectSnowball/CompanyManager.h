//
//  CompanyManager.h
//  projectSnowball
//
//  Created by Donald Fung on 1/17/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

//  Store an instance of Company that will be used to pass company data to any view.

#import <Foundation/Foundation.h>
#import "Company.h"

typedef void (^finished)(BOOL);

@interface CompanyManager : NSObject

@property (nonatomic, strong) Company *company;

+ (id)sharedCompany;

//  Assigns a company some new data
- (void)newCompany:(NSDictionary *)data;

//  Loads an existing/saved company to view
- (void)existingCompany:(Company *)company;

//  Checks if the company selected is saved in the watchlist
- (BOOL)isFollowing:(Company *)company;

//  Gets saved company from Core Data
- (Company *)getSavedCompany:(Company *)company;

//  Updates selected company if it was saved.  Checks for reachability first
- (void)updateSavedCompany:(Company *)savedCompany;

//  checks for reachability first.  If connection is available, send GET request.  If no connection is available, load from Core Data.
- (void)prepareRequest:(Company *)company;

@end
