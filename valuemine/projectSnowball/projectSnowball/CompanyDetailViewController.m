
//
//  CompanyDetailViewController.m
//  projectSnowball
//
//  Created by Blayne Chong on 2015-11-10.
//  Copyright © 2015 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "CompanyDetailViewController.h"
#import "TenYearTableViewController.h"
#import "OneYearTableViewController.h"
#import "CompanyDetailTableHeaderViewController.h"
#import "TenYearDetailTableViewController.h"
#import "SummaryTableViewController.h"
#import "TenYearDetailViewController.h"

#define ratioVcID @"RatioVC"
#define oneYearTvCID @"oneYearTvCIdentifier"
#define tenYearTvCID @"TenYearTableViewControllerIdentifier"
#define summaryVcID @"summaryTvCIdentifier"

typedef NS_ENUM(int, segmentedIndex)
{
    year_to_date = 0,
    ten_year = 1,
};

@interface CompanyDetailViewController () <UIScrollViewDelegate, BaseTableDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;
@property (weak, nonatomic) IBOutlet UIView *topContentHolder;
@property (nonatomic, strong) IBOutlet UIView *financialTableHolder;
@property (nonatomic, strong) OneYearTableViewController *oneYearTvC;
@property (nonatomic, strong) TenYearTableViewController *tenYearTvC;
@property (nonatomic, strong) NSDictionary *lastCompanySelected;
@property (nonatomic, assign) segmentedIndex index_selected;
@property (nonatomic, strong) TenYearDetailViewController *destViewController;
@property (nonatomic, strong) CompanyDetailTableHeaderViewController *companyDetailTVC;
@property (nonatomic, strong) SummaryTableViewController *summaryTVC;
@property (weak, nonatomic) IBOutlet UIView *backdropView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *followButton;
@property (nonatomic, assign) BOOL isConnected;

@end

@implementation CompanyDetailViewController

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lastCompanySelected = [[NSDictionary alloc]init];
    self.companyManager = [CompanyManager sharedCompany];
    self.isConnected = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self configureNotificationForCompanyData];
    [self deselectCell];
    [self reloadView];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

#pragma mark Notifications

- (void)configureNotificationForCompanyData
{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processNewCompanyData)
                                                 name:newCompanyData
                                               object:nil];
}

- (void)configureNotificationForSegmentedControl
{
    //  Add observer to listen for when the segmented control is tapped/pressed
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(segmentedControlDidChange:)
                                                 name:indexChanged
                                               object:nil];
}

- (void)processNewCompanyData
{
    [self setupNavBar];
    [self setupSummaryView];
    [self setupTableView];
    [self changeStateOfFollowButton];
    [self configureNotificationForSegmentedControl];
    self.isConnected = YES;
}

- (void)reloadView
{
    Company *company = [self getCompanyDataForViewController];
    if (company != nil) {
        [self configureNotificationForSegmentedControl];
    }
    
    [self changeStateOfFollowButton];
}

- (void)deselectCell
{
    if (self.tenYearTvC != nil) {
        NSIndexPath *indexPath = [self.tenYearTvC.tableView indexPathForSelectedRow];
        [self.tenYearTvC.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

#pragma mark Title

- (void)setupNavBar
{
    Company *company = [self getCompanyDataForViewController];
    [self setNavBarTitle:company.ticker];
    self.backdropView.backgroundColor = [UIColor darkBlue];
}

#pragma mark Header View - Company Name, Ticker, Price
- (void)setupSummaryView
{
    if (self.companyDetailTVC == nil) {
        //  the top content view contains stuff like price, company, ticker, and ratios
        self.topContentHolder.backgroundColor = [UIColor darkBlue];
        self.companyDetailTVC = [self.storyboard instantiateViewControllerWithIdentifier:companyDetailTableHeaderControllerIdentifier];
        self.companyDetailTVC.view.frame = CGRectMake(0, 0, self.topContentHolder.frame.size.width, self.topContentHolder.frame.size.height);
        [self.topContentHolder addSubview:self.companyDetailTVC.view];
    }
}

#pragma mark Scroll View
- (UIScrollView *)getScrollViewContentSize:(CGFloat)tableHeight
{
    /*  calculate height of content for scroll view.  Set it equal to the height of the top content holder plus the height of the tableview.  The top content holder contains ratio info, company price, ticker, name and the segmented control */
    CGFloat contentSize = self.topContentHolder.frame.size.height + tableHeight;
    self.scrollview.contentSize = CGSizeMake(self.scrollview.frame.size.width, contentSize);
    return self.scrollview;
}

#pragma mark Segmented Control

- (void)segmentedControlDidChange:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.userInfo;
    int index = [[userInfo objectForKey:@"index"] intValue];
    self.index_selected = index;
    [self getIndexTapped:self.index_selected];
}

- (void)getIndexTapped:(NSUInteger)index
{
    
    switch (index) {
        case year_to_date:
            [self selectedIndex:year_to_date];
            break;
        case ten_year:
            [self selectedIndex:ten_year];
            break;
        default:
            break;
    }
}

- (void)selectedIndex:(int)index
{
    //  loads the respective tableview based on the tab selected in the segmented control
    self.index_selected = index;
    if (self.companyManager.company.revenues != nil)
    {
        [self setupTableView];
    }
}

#pragma mark Tableview setup

- (void)setupTableView
{
    //  if segmented index is 0, load 1 yr view
    if (self.index_selected == year_to_date) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self loadOneYearTableview];
        });
        
    }
    // if segmented index is set to 1, load 10 yr view
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self loadTenYearTableview];
        });
        
    }
}

- (void)loadOneYearTableview
{
    self.oneYearTvC = [self.storyboard instantiateViewControllerWithIdentifier:oneYearTvCID];
    [self.financialTableHolder addSubview:self.oneYearTvC.view];
    [self.tenYearTvC.view removeFromSuperview];
    CGFloat tableHeight = self.oneYearTvC.tableView.contentSize.height;
    self.oneYearTvC.view.frame = CGRectMake(0, 0, self.financialTableHolder.frame.size.width, tableHeight);
    [self getScrollViewContentSize:tableHeight];
}

- (void)loadTenYearTableview
{
    //  initialize tableview controller for view
    self.tenYearTvC = [self.storyboard instantiateViewControllerWithIdentifier:tenYearTvCID];
    self.tenYearTvC.baseTableDelegate = self;
    self.tenYearTvC.tableView.scrollEnabled = YES;
    [self.financialTableHolder addSubview:self.tenYearTvC.view];
    [self.oneYearTvC.view removeFromSuperview];
    CGFloat tableHeight = self.tenYearTvC.tableView.contentSize.height;
    self.tenYearTvC.view.frame = CGRectMake(0, 0, self.financialTableHolder.frame.size.width, tableHeight);
    [self getScrollViewContentSize:tableHeight];
}

#pragma mark - Buttons

- (IBAction)backButton:(id)sender
{
    Company *company = [self getCompanyDataForViewController];
    if (company.is_following) {
        [[CompanyManager sharedCompany] updateSavedCompany:company];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:UIKeyboardWillHideNotification object:self];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Follow Button

- (IBAction)followButtonTapped:(id)sender
{
    if ([self.baseViewDelegate respondsToSelector:@selector(addOrRemoveCompanyFromWatchlist)]) {
        [self.baseViewDelegate addOrRemoveCompanyFromWatchlist];
    }
}

- (void)changeStateOfFollowButton
{
    Company *company = [self getCompanyDataForViewController];
    if (company.is_following) {
        [self changeButtonToTick];
    }
    else
    {
        [self changeButtonToAdd];
    }
}

- (void)addOrRemoveCompanyFromWatchlist
{
    if (self.isConnected) {
        Company *company = [self getCompanyDataForViewController];
        if (company.is_following)
        {
            [[CoreDataManager coreDataManager] deleteCompany:company];
            self.companyManager = [CompanyManager sharedCompany];
            self.companyManager.company.is_following = NO;
            [self changeButtonToAdd];
        }
        else
        {
            [[CoreDataManager coreDataManager] saveCompany:company];
            self.companyManager.company.is_following = YES;
            [self changeButtonToTick];
        }
    }
}


- (void)changeButtonToTick
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.followButton.image = [UIImage imageNamed:tick];
    });
}

- (void)changeButtonToAdd
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.followButton.image = [UIImage imageNamed:add];
    });
}

#pragma mark Base Tableview Delegate

- (void)cellPressed
{
    /*  Handles the segue for the 10 yr detail table view.  It has to be done this way because the 10 yr table view is a subview of the company detail view controller and hence not directly connected to the navigation controller.  Therefore it can't perform push segues.  This delegate method tells the company detail view controller to perform the segue on behalf of the 10 yr view controller.  Note that I had to initialize and perform all the data setting in the prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender method below.  */
    [self performSegueWithIdentifier:companyDetail_tenYearDetail_vc sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:companyDetail_tenYearDetail_vc])
    {
        //  gets an instance of company.
        Company *company = [self getCompanyDataForViewController];
        
        //  gets the index path that was pressed
        NSIndexPath *indexPath = [self.tenYearTvC.tableView indexPathForSelectedRow];
        
        //  gets some relevant data used to populate the 10yr detailed view we are going to push to
        [self.tenYearTvC getDataForTable:indexPath];
        
        // Get reference to the destination view controller
        self.destViewController = [segue destinationViewController];
        //  assign some values to properties of this view
        self.destViewController.tenYearData = self.tenYearTvC.figures;
        self.destViewController.sectionTitle = self.tenYearTvC.figuresCategory;
        self.destViewController.topNavTitle = company.ticker;
    }
}


@end
