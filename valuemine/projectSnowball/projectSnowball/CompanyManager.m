//
//  CompanyManager.m
//  projectSnowball
//
//  Created by Donald Fung on 1/17/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "CompanyManager.h"
#import "Constants.h"
#import "CoreDataManager.h"
#import "ServerClient.h"
#import "ReachabilityManager.h"

@interface CompanyManager ()

@property (nonatomic, assign) BOOL updateCompanyTriggered;
@end
@implementation CompanyManager

+ (id)sharedCompany
{
    static CompanyManager *sharedCompany = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedCompany = [[self alloc] init];
    });
    return sharedCompany;
}

#pragma mark Notifications

- (void)configureNotification
{
    [[NSNotificationCenter defaultCenter] postNotificationName:newCompanyData object:self];
}

#pragma mark Assign company data

- (void)newCompany:(NSDictionary *)data
{
    self.company = [[Company alloc]init];
    NSDictionary *companyData = [data valueForKey:@"companyData"];
    NSDictionary *currentStockPrice = [data valueForKey:@"currentPrice"];
    
    //  Give Company new data
    self.company.tenYearData = companyData;
    self.company.name = [companyData valueForKey:@"companyName"];
    self.company.ticker = [companyData valueForKey:@"companyTicker"];
    self.company.revenues = [self getCompanyDataFromDictionary:companyData withKey:@"REVENUE"];
    self.company.fiscalYearEndDate = [self getFiscalYearEndDate:companyData withKey:@"REVENUE"];
    self.company.netIncome = [self getCompanyDataFromDictionary:companyData withKey:@"NETINC"];
    self.company.cashAndCashEquivalents = [self getCompanyDataFromDictionary:companyData withKey:@"CASHNEQ"];
    self.company.currentAssets = [self getCompanyDataFromDictionary:companyData withKey:@"ASSETSC"];
    self.company.intangibleAssets = [self getCompanyDataFromDictionary:companyData withKey:@"INTANGIBLES"];
    self.company.totalAssets = [self getCompanyDataFromDictionary:companyData withKey:@"ASSETS"];
    self.company.longTermDebt = [self getCompanyDataFromDictionary:companyData withKey:@"DEBT"];
    self.company.otherLongTermLiabilities = [self getCompanyDataFromDictionary:companyData withKey:@"LIABILITIESNC"];
    self.company.totalLiabilities = [self getCompanyDataFromDictionary:companyData withKey:@"LIABILITIES"];
    self.company.sharesOutstanding = [self getCompanyDataFromDictionary:companyData withKey:@"SHARESWA"];
    self.company.retainedEarnings = [self getCompanyDataFromDictionary:companyData withKey:@"RETEARN"];
    self.company.shareholdersEquity = [self getCompanyDataFromDictionary:companyData withKey:@"EQUITY"];
    self.company.dividends = [self getCompanyDataFromDictionary:companyData withKey:@"NCFDIV"];
    self.company.dividendsPerShare = [self getCompanyDataFromDictionary:companyData withKey:@"DPS"];
    self.company.inventory = [self getCompanyDataFromDictionary:companyData withKey:@"INVENTORY"];
    self.company.earningsPerShare = [self getCompanyDataFromDictionary:companyData withKey:@"EPS"];
    self.company.currentLiabilities = [self getCompanyDataFromDictionary:companyData withKey:@"LIABILITIESC"];
    self.company.stockPrice = [self currentStockPrice:companyData withKey:@"PRICE" forCurrentPriceDictionary:currentStockPrice withKey:@"closePrice"];
    
    //self.company.stockPriceHistory = ;          Assign dictionary value from server here
    
    self.company.fiscalYearEndPrice = [self getCompanyDataFromDictionary:companyData withKey:@"PRICE"];
    
    NSDictionary *ratioData= [self ratios:companyData];
    self.company.priceToEarningsRatio = [ratioData valueForKey:pe_ratio];
    self.company.priceToBookRatio = [ratioData valueForKey:price_to_book_value];
    self.company.currentRatio = [ratioData valueForKey:current_ratio];
    self.company.pegRatio = [ratioData valueForKey:price_earnings_growth];
    self.company.debtToCapitalRatio = [ratioData valueForKey:debt_to_capital_ratio];
    self.company.quickRatio = [ratioData valueForKey:quick_ratio];
    self.company.dividendsPerShare = [ratioData valueForKey:dividends_per_share];
    self.company.bookValuePerShare = [ratioData valueForKey:book_value_per_share];
    self.company.bookValue = [ratioData valueForKey:book_value];
    self.company.netCurrentAssetValuePerShare = [ratioData valueForKey:net_current_asset_value];
    
    //  is following?
    self.company.is_following = [self isFollowing:self.company];
    if (self.updateCompanyTriggered) {
        [[CoreDataManager coreDataManager]saveCompany:self.company];
        self.updateCompanyTriggered = NO;
    }
    
    [self configureNotification];
}

- (void)existingCompany:(Company *)company
{
    self.company = [[Company alloc]init];
    self.company.tenYearData = company.tenYearData;
    self.company.name = company.name;
    self.company.ticker = company.ticker;
    self.company.revenues = company.revenues;
    self.company.fiscalYearEndDate = company.fiscalYearEndDate;
    self.company.netIncome = company.netIncome;
    self.company.cashAndCashEquivalents = company.cashAndCashEquivalents;
    self.company.currentAssets = company.currentAssets;
    self.company.intangibleAssets = company.intangibleAssets;
    self.company.totalAssets = company.totalAssets;
    self.company.longTermDebt = company.longTermDebt;
    self.company.otherLongTermLiabilities = company.otherLongTermLiabilities;
    self.company.totalLiabilities = company.totalLiabilities;
    self.company.sharesOutstanding = company.sharesOutstanding;
    self.company.retainedEarnings = company.retainedEarnings;
    self.company.shareholdersEquity = company.shareholdersEquity;
    self.company.dividends = company.dividends;
    self.company.dividendsPerShare = company.dividendsPerShare;
    self.company.inventory = company.inventory;
    self.company.earningsPerShare = company.earningsPerShare;
    self.company.currentLiabilities = company.currentLiabilities;
    self.company.stockPrice = company.stockPrice;
    self.company.fiscalYearEndPrice = company.fiscalYearEndPrice;
    self.company.priceToEarningsRatio = company.priceToEarningsRatio;
    self.company.priceToBookRatio = company.priceToBookRatio;
    self.company.currentRatio = company.currentRatio;
    self.company.pegRatio = company.pegRatio;
    self.company.debtToCapitalRatio = company.debtToCapitalRatio;
    self.company.quickRatio = company.quickRatio;
    self.company.dividendsPerShare = company.dividendsPerShare;
    self.company.bookValuePerShare = company.bookValuePerShare;
    self.company.bookValue = company.bookValue;
    self.company.netCurrentAssetValuePerShare = company.netCurrentAssetValuePerShare;
    self.company.is_following = company.is_following;
    
    [self configureNotification];
}

#pragma mark Ratio Calculation & Other

- (NSMutableDictionary *)ratios:(NSDictionary *)companyData
{
    NSString *fiscalYearEndDate = self.company.fiscalYearEndDate;
    NSNumber *totalAsset = [self valueForKey:totalAssets_key fromDict:companyData];
    NSNumber *totalLiabilities = [self valueForKey:totalLiabilities_key fromDict:companyData];
    NSNumber *intangibleAsset = [self valueForKey:intangibles_key fromDict:companyData];
    NSNumber *numSharesOutstanding = [self valueForKey:sharesOutstanding_key fromDict:companyData];
    NSNumber *debt = [self valueForKey:debt_key fromDict:companyData];
    NSNumber *equity = [self valueForKey:equity_key fromDict:companyData];
    NSNumber *inventoryItem = [self valueForKey:inventory_key fromDict:companyData];
    NSNumber *currentAsset = [self valueForKey:currentAssets_key fromDict:companyData];
    NSNumber *currentLiability = [self valueForKey:currentLiab_key fromDict:companyData];
    NSNumber *dividendsPerShare = [self valueForKey:dividendsPerShare_key fromDict:companyData];
    NSNumber *earningsPerShare = [self valueForKey:eps_key fromDict:companyData];
    NSNumber *fiscalYearEndPrice = [self yearEndPriceForKey:price_key fromDict:companyData andYearEndDate:fiscalYearEndDate];
    
    NSNumber *priceToEarningsRatio = [NSNumber numberWithDouble:([fiscalYearEndPrice doubleValue] / [earningsPerShare doubleValue])];
    NSNumber *bookVal = [NSNumber numberWithDouble:([totalAsset doubleValue] - ([intangibleAsset doubleValue] + [totalLiabilities doubleValue]))];
    NSNumber *book_ValuePerShare = [NSNumber numberWithDouble:([bookVal doubleValue] / [numSharesOutstanding doubleValue])];
    NSNumber *pbValue = [NSNumber numberWithDouble:([fiscalYearEndPrice doubleValue] / [book_ValuePerShare doubleValue])];
    NSNumber *debtToCapRatio = [NSNumber numberWithDouble:([debt doubleValue] / ([debt doubleValue] + [equity doubleValue]))];
    NSNumber *peg = [NSNumber numberWithDouble:([priceToEarningsRatio doubleValue])];
    NSNumber *current_Ratio = [self getCurrentRatioFromCurrentAssets:currentAsset andCurrentLiabilities:currentLiability];
    NSNumber *qckRatio = [self getQuickRatioFromCurrentAssets:currentAsset andCurrentLiabilities:currentLiability andInventory:inventoryItem];
    NSNumber *ncav = [self getNcavFromCurrentAssets:currentAsset andTotalLiabilities:totalLiabilities];
    
    //  Dictionary to contain ratios for each new company
    NSMutableDictionary *companyRatios = [[NSMutableDictionary alloc]init];
    [companyRatios setValue:priceToEarningsRatio forKey:pe_ratio];
    [companyRatios setValue:bookVal forKey:book_value];
    [companyRatios setValue:book_ValuePerShare forKey:book_value_per_share];
    [companyRatios setValue:pbValue forKey:price_to_book_value];
    [companyRatios setValue:debtToCapRatio forKey:debt_to_capital_ratio];
    [companyRatios setValue:current_Ratio forKey:current_ratio];
    [companyRatios setValue:qckRatio forKey:quick_ratio];
    [companyRatios setValue:dividendsPerShare forKey:dividends_per_share];
    [companyRatios setValue:ncav forKey:net_current_asset_value];
    [companyRatios setValue:peg forKey:price_earnings_growth];
    return companyRatios;
}

/*  Retrieves the respective figure or item from a dictionary.  This is used for items that will be assigned to the new company as is and requires no further editing.  examples include Revenue and Net Income data. */

- (NSNumber *)getCompanyDataFromDictionary:(NSDictionary *)dictionary withKey:(NSString *)key
{
    NSArray *dataStream = [dictionary valueForKey:key];
    if (dataStream.count == 0) {
        NSNumber *placeholderNum = [NSNumber numberWithDouble:0];
        return placeholderNum;
    }
    else
    {
        NSArray *lastItem = [dataStream objectAtIndex:0];
        NSNumber *figure = [lastItem objectAtIndex:1];
        return figure;
    }
}

//  Retrieves the fiscal year end date

- (NSString *)getFiscalYearEndDate:(NSDictionary *)dictionary withKey:(NSString *)key
{
    NSArray *dataStream = [dictionary valueForKey:key];
    if (dataStream.count == 0) {
        NSString *error = @"-";
        return error;
    }
    else
    {
        NSArray *lastItem = [dataStream objectAtIndex:0];
        NSString *date = [lastItem objectAtIndex:0];
        return date;
    }
}

//  Retrieves the current stock price

- (NSNumber *)currentStockPrice:(NSDictionary *)companyDic withKey:(NSString *)closingPrice forCurrentPriceDictionary:(NSDictionary *)currentPriceDic withKey:(NSString *)price
{
    if ([currentPriceDic valueForKey:price]) {
        return [currentPriceDic valueForKey:price];
    }
    else {
        NSArray *priceDataStream = [companyDic valueForKey:closingPrice];
        NSArray *latestPriceData = [priceDataStream objectAtIndex:0];
        return [latestPriceData objectAtIndex:1];
    }
}

/*  Used to retrieve ratio values out of a dictionary.  Note that it first checks if an object exists and then assigns a placeholder value if it doesn't */

- (NSNumber *)valueForKey:(NSString *)key fromDict:(NSDictionary *)dict
{
    NSNumber *figure;
    NSArray *data = [dict valueForKey:key];
    if (data.count == 0)
    {
        figure = [NSNumber numberWithDouble:0];
    }
    else
    {
        NSArray *lastItem = [data objectAtIndex:0];
        figure = [lastItem objectAtIndex:1];
    }
    return figure;
}

//  Calculates the year end price.

- (NSNumber *)yearEndPriceForKey:(NSString *)key fromDict:(NSDictionary *)companyData andYearEndDate:(NSString *)yearEndDate
{
    NSNumber *yearEndPrice;
    NSArray *data = [companyData valueForKey:key];
    for (int i = 0; i < data.count; i ++)
    {
        NSMutableArray *priceData = [data objectAtIndex:i];
        NSString *date = [priceData objectAtIndex:0];
        if ([yearEndDate isEqualToString: date])
        {
            yearEndPrice = [priceData objectAtIndex:1];
        }
    }
    return yearEndPrice;
}

//  Calculates the current ratio

- (NSNumber *)getCurrentRatioFromCurrentAssets:(NSNumber *)currentAssets andCurrentLiabilities:(NSNumber *)currentLiabilities
{
    NSNumber *currentRatio;
    if (currentAssets == [NSNumber numberWithDouble:0] && currentLiabilities == [NSNumber numberWithDouble:0])
    {
        currentRatio = [NSNumber numberWithDouble:0];
    }
    else
    {
        currentRatio = [NSNumber numberWithDouble:([currentAssets doubleValue] / [currentLiabilities doubleValue])];
    }
    return currentRatio;
}

//  Calculates the quick ratio

- (NSNumber *)getQuickRatioFromCurrentAssets:(NSNumber *)currentAssets andCurrentLiabilities:(NSNumber *)currentLiabilities andInventory:(NSNumber *)inventory
{
    NSNumber *quickRatio;
    if (currentAssets == [NSNumber numberWithDouble:0] && currentLiabilities == [NSNumber numberWithDouble:0])
    {
        quickRatio = [NSNumber numberWithDouble:0];
    }
    else
    {
        quickRatio = [NSNumber numberWithDouble:(([currentAssets doubleValue] - [inventory doubleValue]) / [currentLiabilities doubleValue])];
    }
    return quickRatio;
}

//  Calculates the Net Current Asset Value (NCAV)

- (NSNumber *)getNcavFromCurrentAssets:(NSNumber *)currentAssets andTotalLiabilities:(NSNumber *)totalLiabilities
{
    NSNumber *ncav;
    if (currentAssets == [NSNumber numberWithDouble:0] && totalLiabilities == [NSNumber numberWithDouble:0])
    {
        ncav = [NSNumber numberWithDouble:0];
    }
    else
    {
        ncav = [NSNumber numberWithDouble:([currentAssets doubleValue] - [totalLiabilities doubleValue])];
    }
    return ncav;
}




#pragma mark Update Financial Data

//  Checks to see if the company is being followed or not

- (BOOL)isFollowing:(Company *)company
{
    NSArray *objects = [[CoreDataManager coreDataManager] fetchCompanies];
    if (objects.count != 0)
    {
        for (Company *companyFollowed in objects) {
            if ([company.ticker isEqualToString:companyFollowed.ticker]) {
                company.is_following = companyFollowed.is_following;
                return company.is_following;
            }
        }
    }
    else
    {
        NSLog(@"No companies followed");
    }
    return company.is_following;
}

//  Returns the saved company.  Used as a supporting function for updating the company's financial data when the view refreshes

- (Company *)getSavedCompany:(Company *)company
{
    Company *savedCompany;
    NSArray *objects = [[CoreDataManager coreDataManager] fetchCompanies];
    if (objects.count != 0)
    {
        for (Company *retrievedCompany in objects) {
            if ([company.ticker isEqualToString:retrievedCompany.ticker]) {
                savedCompany = retrievedCompany;
            }
        }
    }
    return savedCompany;
}

//  Checks for reachability, before triggering a call to update the financial data for the company

- (void)updateSavedCompany:(Company *)savedCompany
{
    
    BOOL isReachable = [ReachabilityManager isReachable];
    if (isReachable) {
        NSLog(@"connection available");
        Company *company = [self getSavedCompany:savedCompany];
        [[CoreDataManager coreDataManager]updateCompany:company finished:^(BOOL success)
         {
             self.updateCompanyTriggered = YES;
             [self makeCall:savedCompany];
         }];
    }
    else
    {
        NSLog(@"no connection");
    }
}

//  Added functionality for updating financial data when there's no internet.  Company data is loaded from core data if this is the case

- (void)prepareRequest:(Company *)company
{
    BOOL isReachable = [ReachabilityManager isReachable];
    if (isReachable) {
        NSLog(@"connection available");
        [self makeCall:company];
    }
    else
    {
        NSLog(@"no connection");
        BOOL companySaved = [self isFollowing:company];
        if (companySaved) {
            Company *savedCompany = [self getSavedCompany:company];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self existingCompany:savedCompany];
                NSLog(@"%@ loaded from Core Data",company.name);
            });
        }
    }
}

//  Calls method to handle the fetching of company data from server

-(void)makeCall:(Company *)company
{
    [[ServerClient sharedClient]fetchCompanyWithName:company.name andTicker:company.ticker];
}


@end
