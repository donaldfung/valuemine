//
//  MainCustomTableViewCell.h
//  projectSnowball
//
//  Created by Blayne Chong on 2015-11-19.
//  Copyright © 2015 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WishListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *tickerSymbolLabel;
@property (weak, nonatomic) IBOutlet UILabel *figureForCell;

@end
