//
//  NoCompaniesSavedViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 1/15/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "EmptyWatchListViewController.h"
#import "SearchViewController.h"

#define message @"Your watchlist is empty.  Add some companies to track their financials."

@interface EmptyWatchListViewController ()

@property (weak, nonatomic) IBOutlet UILabel *emptyMessage;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@end
@implementation EmptyWatchListViewController


#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [self setupLabel];
}

#pragma mark - Configure UI

- (void)setUpButton
{
    [self.addButton setImage:[UIImage imageNamed:followButtonImage] forState:UIControlStateNormal];
}

- (void)setupLabel
{
    self.emptyMessage.text = message;
    self.emptyMessage.textColor = [UIColor defaultTheme];
    self.emptyMessage.font = [self setFontToHelveticaWithSizeFourteen];
    self.emptyMessage.numberOfLines = 3;
}

#pragma mark Buttons

- (IBAction)addButtonPressed:(id)sender
{
    if ([self.baseViewDelegate respondsToSelector:@selector(transitionToViewController)]) {
        [self.baseViewDelegate transitionToViewController];
    }
}
@end
