//
//  TenYearDetailedTableViewCell.m
//  projectSnowball
//
//  Created by Donald Fung on 1/24/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "TenYearDetailedTableViewCell.h"
#import "UIColor+customColors.h"

@interface TenYearDetailedTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

@end
@implementation TenYearDetailedTableViewCell

- (void)dataForCell:(NSArray *)tableData withSectionTitle:(NSString *)title andIndexPath:(NSIndexPath *)indexPath
{
    NSArray *tableDataElement = [tableData objectAtIndex:indexPath.row];
    NSNumber *num = [tableDataElement objectAtIndex:1];
    NSString *figure;
    if ([title isEqualToString:sharesoutstanding_label]) {
        figure = [self stringFromNumberWithDecimalStyle:num];
    }
    else
    {
        figure = [self formatNumberCurrencyStyle:num];
    }
    
    self.rightLabel.text = figure;
    self.rightLabel.font = [self setFontToHelveticaWithSizeFourteen:self.rightLabel];
    self.rightLabel.textColor = [UIColor defaultTheme];
    
    //  Add date here
    NSString *date = [tableDataElement objectAtIndex:0];
    self.leftLabel.text = date;
    self.leftLabel.font = [self setFontToHelveticaWithSizeFourteen:self.leftLabel];
    self.leftLabel.textColor = [UIColor defaultTheme];
}

@end
