//
//  FilterManager.h
//  projectSnowball
//
//  Created by Donald Fung on 1/16/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilterManager : NSObject

@property (nonatomic, strong) NSMutableDictionary *dictionaryOfCompanies;
@property (nonatomic, strong) NSMutableDictionary *tickerKeys;
@property (nonatomic, strong) NSMutableArray *tickerAndCompanies;

+ (id)sharedFilter;
- (void)preloadData;
@end
