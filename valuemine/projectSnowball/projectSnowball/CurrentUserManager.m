//
//  NewUsersClient.m
//  projectSnowball
//
//  Created by Donald Fung on 1/10/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "CurrentUserManager.h"

@implementation CurrentUserManager

+ (id)sharedCurrentUserManager
{
    static CurrentUserManager *sharedCurrentUserManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedCurrentUserManager = [[self alloc] init];
    });
    return sharedCurrentUserManager;
}

- (id)init {
    if (self = [super init]) {
//        self.currentUser = [[User alloc]init];
    }
    return self;
}

@end
