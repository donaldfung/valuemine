//
//  OneYearTvCell.h
//  projectSnowball
//
//  Created by Donald Fung on 1/22/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseTableViewCell.h"

@interface OneYearTvCell : BaseTableViewCell    

- (void)dataForCellAtIndexPath:(NSIndexPath *)indexPath withArrayOfRowLabels:(NSArray *)rowLabelsArray;
@end
