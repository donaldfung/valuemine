//
//  SwitchSlideManager.m
//  projectSnowball
//
//  Created by Donald Fung on 1/28/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "SwitchSlideManager.h"
#import "SlideOneViewController.h"
#import "SlideTwoViewController.h"

typedef NS_ENUM(int, slideViewController) {
    slideOne,
    slideTwo = 1
};

@interface SwitchSlideManager ()
@property (nonatomic, strong) SlideOneViewController *slideOneVC;
@property (nonatomic, strong) SlideTwoViewController *slideTwoVC;
@property (nonatomic, assign) slideViewController slideSelected;
@end
@implementation SwitchSlideManager

- (id)initWithCustomFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        self.pageControl = [[UIPageControl alloc]initWithFrame:CGRectMake((self.frame.size.width)*.47, (self.frame.size.height)*.75, 20, 10)];
        
        self.scrollView.pagingEnabled = YES;
        self.scrollView.showsHorizontalScrollIndicator = NO;
        self.scrollView.delegate = self;
        self.scrollView.backgroundColor = [UIColor darkBlue];
        
        self.pageControl.userInteractionEnabled = NO;
        
        [self addSubview:self.scrollView];
        [self addSubview:self.pageControl];
        
    }
    return self;
}

- (void)configureViewWithObjects:(NSArray *)array
{
    self.totalPages = array.count;
    self.pageControl.numberOfPages = self.totalPages;
    self.scrollView.contentSize = CGSizeMake(self.frame.size.width * self.totalPages, self.scrollView.frame.size.height);
    self.currentIndex = 0;
    
    for (int i = 0; i<self.totalPages; i++)
    {
        NSString *identifier = [array objectAtIndex:i];
        switch (i) {
            case slideOne:
                self.slideOneVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:identifier];
                self.slideOneVC.view.frame = CGRectMake(self.frame.size.width*0, 0, self.frame.size.width, self.scrollView.frame.size.height);
                self.slideOneVC.view.contentMode = UIViewContentModeScaleAspectFit;
                [self.scrollView addSubview:self.slideOneVC.view];
                break;
                
            case slideTwo:
                self.slideTwoVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:identifier];
                self.slideTwoVC.view.frame = CGRectMake(self.frame.size.width*1, 0, self.frame.size.width, self.scrollView.frame.size.height);
                self.slideTwoVC.view.contentMode = UIViewContentModeScaleAspectFit;
                [self.scrollView addSubview:self.slideTwoVC.view];
                break;
            default:
                break;
        }
    }
}



@end
