//
//  SignUpViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 1/10/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "SignUpViewController.h"
#import <Parse/Parse.h>
#import "Constants.h"
#import "CurrentUserManager.h"

typedef NS_ENUM(int, Role)
{
   admin = 0,
    user = 1
};
                 ;
@interface SignUpViewController ()
@property (strong, nonatomic) IBOutlet UITextField *emailField;
@property (strong, nonatomic) IBOutlet UITextField *usernameField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) CurrentUserManager *userManager;
@property Role role;
@end

@implementation SignUpViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - Buttons

- (IBAction)signUpPressed:(id)sender
{
    //  New User
    
    PFUser *newUser = [PFUser user];
    [[CurrentUserManager sharedCurrentUserManager] setCurrentUser:newUser];
    self.userManager.currentUser.username = self.usernameField.text;
    self.userManager.currentUser.password = self.passwordField.text;
    self.userManager.currentUser.email = self.emailField.text;
    
    [self.userManager.currentUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error)
        {
            // Sign Up Successful
            NSLog(@"thanks for signing up %@\n", self.userManager.currentUser.username);
            self.userManager.currentUser = [PFUser currentUser];
            
            if (self.userManager.currentUser) {
                // do stuff with the user
                [self role:self.userManager.currentUser];
            } else
            {
                // show the signup or login screen
            }
        }
        else
        {
            //  Sign Up Failed
            NSString *errorString = [error userInfo][@"error"];   // Show the errorString somewhere and let the user try again.
            NSLog(@"%@",errorString);
        }
    }];
}

#pragma mark - Admin/User Check

- (void)role:(PFUser *)newUser
{
    if ([donald_email isEqual: newUser.email] || [blayne_email isEqual: newUser.email]) {
        self.role = admin;
        NSLog(@"admin %@ is logged in", newUser.username);
        
    }
    else {
        self.role = user;
    }
}
@end
