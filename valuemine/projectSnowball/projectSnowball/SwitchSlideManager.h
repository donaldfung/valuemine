//
//  SwitchSlideManager.h
//  projectSnowball
//
//  Created by Donald Fung on 1/28/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "SwitchSlideView.h"

@interface SwitchSlideManager : SwitchSlideView

- (id)initWithCustomFrame:(CGRect)frame;
- (void)configureViewWithObjects:(NSArray *)objects;
@end
