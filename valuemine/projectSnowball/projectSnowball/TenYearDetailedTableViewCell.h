//
//  TenYearDetailedTableViewCell.h
//  projectSnowball
//
//  Created by Donald Fung on 1/24/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface TenYearDetailedTableViewCell : BaseTableViewCell

- (void)dataForCell:(NSArray *)tableData withSectionTitle:(NSString *)title andIndexPath:(NSIndexPath *)indexPath;
@end
