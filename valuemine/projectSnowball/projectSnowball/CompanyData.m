//
//  CompanyData.m
//  
//
//  Created by Donald Fung on 12/5/15.
//
//

#import "CompanyData.h"

@implementation CompanyData

// Insert code here to add functionality to your managed object subclass

@dynamic avgEarningsPerShare;
@dynamic avgPriceToEarningsRatio;
@dynamic bookValue;
@dynamic bookValuePerShare;
@dynamic cashAndCashEquivalents;
@dynamic currentAssets;
@dynamic currentLiabilities;
@dynamic currentRatio;
@dynamic debtToCapitalRatio;
@dynamic dividends;
@dynamic dividendsPerShare;
@dynamic earningsPerShare;
@dynamic fiscalYearEndDate;
@dynamic fiscalYearEndPrice;
@dynamic goodwill;
@dynamic industry;
@dynamic intangibleAssets;
@dynamic inventory;
@dynamic logo;
@dynamic longTermDebt;
@dynamic name;
@dynamic netCurrentAssetValuePerShare;
@dynamic netIncome;
@dynamic otherLongTermLiabilities;
@dynamic pegRatio;
@dynamic priceToBookRatio;
@dynamic priceToEarningsRatio;
@dynamic quickRatio;
@dynamic retainedEarnings;
@dynamic revenues;
@dynamic shareholdersEquity;
@dynamic sharesOutstanding;
@dynamic shortTermDebt;
@dynamic stockPrice;
@dynamic stockPriceHistory;
@dynamic ticker;
@dynamic totalAssets;
@dynamic totalExpenses;
@dynamic totalLiabilities;
@dynamic tenYearData;
@dynamic is_following;
@end
