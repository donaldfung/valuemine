//
//  TenYearDetailTableViewController.h
//  projectSnowball
//
//  Created by Donald Fung on 1/23/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "BaseTableViewController.h"

@interface TenYearDetailTableViewController : BaseTableViewController

@property (nonatomic, strong) NSArray *tableFigures;
@property (nonatomic, strong) NSString *sectionTitle;
@property (nonatomic, strong) NSString *topNavTitle;
@end
