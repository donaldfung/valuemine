//
//  UIFont+customFonts.h
//  projectSnowball
//
//  Created by Donald Fung on 1/30/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (customFonts)

+ (UIFont *)setFontToHelveticaWithSizeFourteen;
+ (UIFont *)setFontToHelveticaWithSizeSixteen;
+ (UIFont *)setFontToHelveticaWithSizeTwentyFour;
@end
