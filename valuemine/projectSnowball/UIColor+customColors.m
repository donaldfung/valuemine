//
//  UIColor+customColors.m
//  projectSnowball
//
//  Created by Donald Fung on 1/15/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "UIColor+customColors.h"

@implementation UIColor (customColors)

+ (UIColor *)skyBlueTheme
{
    UIColor *skyBlue = [UIColor colorWithRed:0 green:(172/255.0) blue:(240/255.0) alpha:1.0];
    return skyBlue;
}

+ (UIColor *)lighterShadeOfSkyBlueTheme
{
    UIColor *lighterSkyBlue = [UIColor colorWithRed:(98/255.0) green:(200/255.0) blue:(245/255.0) alpha:1.0];
    return lighterSkyBlue;
}

+ (UIColor *)lightGrey
{
    UIColor *lightGrey = [UIColor colorWithRed:(242/255.0) green:(242/255.0) blue:(242/255.0) alpha:1.0];
    return lightGrey;
}

+ (UIColor *)darkBlue
{
    UIColor *darkBlue = [UIColor colorWithRed:(60/255.0) green:(70/255.0) blue:(80/255.0) alpha:1.0];
    return darkBlue;
}

+ (UIColor *)lightBlue
{
    UIColor *lightBlue = [UIColor colorWithRed:(226/255.0) green:(231/255.0) blue:(235/255.0) alpha:1.0];
    return lightBlue;
}

+ (UIColor *)segmentedControlBackground
{
    UIColor *darkBlue = [UIColor colorWithRed:(107/255.0) green:(114/255.0) blue:(122/255.0) alpha:1.0];
    return darkBlue;
}

+ (UIColor *)defaultTheme
{
    UIColor *label = [UIColor colorWithRed:(67/255.0) green:(67/255.0) blue:(67/255.0) alpha:1.0];
    return label;
}
@end
