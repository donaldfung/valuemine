//
//  BaseTableViewCell.h
//  projectSnowball
//
//  Created by Donald Fung on 1/23/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompanyManager.h"
#import "UIFont+customFonts.h"  
#import "UIColor+customColors.h"    
#import "Constants.h"

@interface BaseTableViewCell : UITableViewCell
@property (strong, nonatomic) CompanyManager *companyManager;

// Number formatter
- (NSString *)formatNumberCurrencyStyleForPricesAndRatios:(NSNumber *)number;
- (NSString *)formatNumberCurrencyStyle:(NSNumber *)number;
- (NSString *)formatNumberNoStyle:(NSNumber *)number;
- (NSString *)stringFromNumberWithDecimalStyle:(NSNumber *)number;

// Font
- (UIFont *)setFontToHelveticaWithSizeTwelve:(UILabel *)label;
- (UIFont *)setFontToHelveticaWithSizeFourteen:(UILabel *)label;

// Company Data
- (Company *)getCompanyDataForCell;

// Labels
- (UILabel *)customizeLabelsForTenYearTableView:(UITableViewCell *)cell;
@end
