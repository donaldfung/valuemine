//
//  Constants.m
//  projectSnowball
//
//  Created by Donald Fung on 11/11/15.
//  Copyright © 2015 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "Constants.h"

@interface Constants ()


@end
@implementation Constants

- (NSMutableArray *) arrayOfQuandlStrings
{
    _urlArrray = [[NSMutableArray alloc]init];
    [self.urlArrray addObject:AnnualRevenues];
    [self.urlArrray addObject:NetIncome];
    //[self.urlArrray addObject:CostOfRevenue];
    [self.urlArrray addObject:DividendsPerShare];
    [self.urlArrray addObject:RetainedEarnings];
    [self.urlArrray addObject:Cash];
    [self.urlArrray addObject:WeightedSharesDiluted];
    [self.urlArrray addObject:GoodWillAndIntangibleAssets];
    [self.urlArrray addObject:CurrentAssets];
    [self.urlArrray addObject:TotalAssets];
    [self.urlArrray addObject:TotalDebt];
    [self.urlArrray addObject:LongTermLiabilities];
    [self.urlArrray addObject:TotalLiabilities];
    [self.urlArrray addObject:EarningsPerShare];
    [self.urlArrray addObject:ShareholdersEquity];
    [self.urlArrray addObject:HistoricalPrices];
    [self.urlArrray addObject:Dividends];
    [self.urlArrray addObject:INVENTORY];
    [self.urlArrray addObject:CURRENTLIABILITIES];
    [self.urlArrray addObject:MarketCap];
    return self.urlArrray;
}
@end
