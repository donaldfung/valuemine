//
//  SearchViewController.m
//  projectSnowball
//
//  Created by Donald Fung on 10/16/15.
//  Copyright © 2015 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchViewControllerCell.h"
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>
#import "CompanyDetailViewController.h"
#import "CoreDataManager.h"
#import "Company.h"
#import "ParseClient.h"
#import "UIColor+customColors.h"
#import "SearchTableViewController.h"
#import "ParseClient.h"
#import "CompanyManager.h"

#define SearchTitle @"Search"

@interface SearchViewController () <SearchTableDelegate,UISearchControllerDelegate, UISearchBarDelegate, NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDataDelegate, NSURLSessionDownloadDelegate>

{
    CGFloat screenWidth;
    CGFloat screenHeight;
}
//  Backend - these properties used to store copies of data from backend.
@property (nonatomic, copy) NSArray *tickersAndCompaniesData;
@property (nonatomic, copy) NSDictionary *companyDictionaryData;
@property (nonatomic, strong) NSMutableArray *companiesForTableView;
@property (nonatomic, strong) NSDictionary *tickerKeyData;
@property (nonatomic, strong) CoreDataManager *coreDataManager;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UIView *searchAndFilterHolder;
@property (strong, nonatomic) IBOutlet UIView *tableHolder;
@property (nonatomic, strong) SearchTableViewController *searchTVC;
@end

@implementation SearchViewController

#pragma mark View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setNavBarTitle:SearchTitle];
    [self setUpSearchAndFilter];
    [self addTableViewAsSubview];
    [self setupContentView];
  
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [self.searchBar becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.searchBar resignFirstResponder];
}

#pragma mark Configure UI
- (void)setupContentView
{
    //  Size of content
    CGRect screenRectangle = [[UIScreen mainScreen] bounds];
    screenWidth = screenRectangle.size.width;
    screenHeight = screenRectangle.size.height;
}


#pragma mark - Search

- (void)setUpSearchAndFilter
{
    self.searchAndFilterHolder.backgroundColor = [UIColor darkBlue];
    _figures = [[NSMutableArray alloc]init];
    self.coreDataManager = [CoreDataManager coreDataManager];
    self.tickersAndCompaniesData = [self.coreDataManager.tickerAndCompanies copy];
    self.companyDictionaryData = [self.coreDataManager.dictionaryOfCompanies copy];
    self.tickerKeyData = [self.coreDataManager.tickerKeys mutableCopy];
    _companiesForTableView = [[NSMutableArray alloc]init];
    self.searchBar.searchBarStyle = UIBarStyleDefault;
    self.searchBar.barStyle = UIBarStyleDefault;
    self.searchBar.barTintColor = [UIColor darkBlue];
    [self.searchBar setTintColor:[UIColor grayColor]];
    
    //  remove some unwanted lines above and below search bar
    CGRect rect = self.searchBar.frame;
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, rect.size.height-2,rect.size.width, 2)];
    UIView *topLineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,rect.size.width, 7)];
    topLineView.backgroundColor = [UIColor darkBlue];
    lineView.backgroundColor = [UIColor darkBlue];
    [self.searchBar addSubview:lineView];
    [self.searchBar addSubview:topLineView];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self searchFilter:searchText];
}


-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

#pragma mark TableView 

- (void)addTableViewAsSubview
{
    //  Size of content
    CGRect rect = self.navigationController.navigationBar.frame;
    float y = rect.size.height;
    self.tableHolder.frame = CGRectMake(0.0, y, screenWidth, screenHeight);

    self.searchTVC = [self.storyboard instantiateViewControllerWithIdentifier:search_tvc_Identifier];
    self.searchTVC.searchTableDelegate = self;
    self.searchTVC.view.frame = CGRectMake(0.0, 0.0, screenWidth, screenHeight);
    [self addChildViewController:self.searchTVC];
    [self.tableHolder addSubview:self.searchTVC.view];
    [self.searchTVC didMoveToParentViewController:self];
}

#pragma mark search filter

- (void)searchFilter:(NSString *)input
{
        NSMutableArray *tempArrayOfResults = [[NSMutableArray alloc]init];
    
        [self.companiesForTableView removeAllObjects];
    
        //  Filter input for best matches
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        if (self.tickersAndCompaniesData != nil)
        {
    
            if (![input  isEqual: @""])
            {
                //  Wild Card Matches Of Tickers
                NSString *formattedInput = [input stringByAppendingString:@"*"];
    
                //  Filter for 1 character Tickers
                if (input.length == 1)
                {
                    NSPredicate *wildCardPredicate = [NSPredicate predicateWithFormat:@"SELF matches[c] %@", input];
                    NSArray *result = [self.tickersAndCompaniesData filteredArrayUsingPredicate:wildCardPredicate];
                    [tempArrayOfResults addObjectsFromArray:result];
                }
                if (input.length == 2)
                {
                    NSPredicate *wildCardPredicate = [NSPredicate predicateWithFormat:@"SELF like[c] %@", formattedInput];
                    NSArray *result = [self.tickersAndCompaniesData filteredArrayUsingPredicate:wildCardPredicate];
                    [tempArrayOfResults addObjectsFromArray:result];
    
                }
                if (input.length == 3)
                {
                    NSPredicate *wildCardPredicate = [NSPredicate predicateWithFormat:@"SELF like[c] %@", formattedInput];
                    NSArray *result = [self.tickersAndCompaniesData filteredArrayUsingPredicate:wildCardPredicate];
                    [tempArrayOfResults addObjectsFromArray:result];
                }
                if (input.length == 4)
                {
                    NSPredicate *wildCardPredicate = [NSPredicate predicateWithFormat:@"SELF like[c] %@", formattedInput];
                    NSArray *result = [self.tickersAndCompaniesData filteredArrayUsingPredicate:wildCardPredicate];
                    [tempArrayOfResults addObjectsFromArray:result];
                }
                else if (input.length > 4)
                {
    
                    NSPredicate *wildCardPredicate = [NSPredicate predicateWithFormat:@"SELF like[c] %@", formattedInput];
                    NSArray *result = [self.tickersAndCompaniesData filteredArrayUsingPredicate:wildCardPredicate];
                    [tempArrayOfResults addObjectsFromArray:result];
                }
            }
    
            if (tempArrayOfResults.count < 1)
            {
                [self.companiesForTableView removeAllObjects];
            }
    
            for (NSString *strings in tempArrayOfResults)
            {
                if (self.companyDictionaryData)
                {
    
                    NSString *nameCheck = [self.companyDictionaryData objectForKey:strings];
    
                    //  If value doesn't exists in companyDicionaryData, check tickerKeyData array for it.
    
                    if (nameCheck == nil) {
                        if ([self.tickerKeyData objectForKey:strings]) {
                            NSMutableArray *arrayOfData = [[NSMutableArray alloc]init];
                            [arrayOfData addObject:[self.tickerKeyData objectForKey:strings]];
                            Company *company = [[Company alloc]init];
    
    
                            for (NSString *stringOfContent in arrayOfData)
                            {
                                company.name = stringOfContent;
                                company.ticker = strings;
    
                                if (self.companiesForTableView.count < 1)
                                {
                                    [self.companiesForTableView addObject:company];
                                }
                                else {
                                    NSString *checkTicker = company.ticker;
                                    //                                NSString *checkName = company.name;
                                    BOOL absent = NO;
                                    for (Company *companies in self.companiesForTableView)
                                    {
    
                                        if (checkTicker != companies.ticker)
                                        {
                                            absent = YES;
    
                                        }
                                        else {
                                            //do nothing
                                            absent = NO;
                                        }
    
                                    }
                                    if (absent == YES) {
                                        [self.companiesForTableView addObject:company];
                                    }
    
                                }
                            }
                        }
    
                    }
    
                    else
                    {
                        //  check for nested objects within the dictionary
    
                        if ([self.companyDictionaryData objectForKey:strings])
                        {
                            NSMutableArray *arrayofData = [[NSMutableArray alloc]init];
                            [arrayofData addObject:[self.companyDictionaryData objectForKey:strings]];
                            NSArray *getObjectsWithinArray = [arrayofData firstObject];
                            Company *company = [[Company alloc]init];
    
    
                            for (int i = 0; i < getObjectsWithinArray.count; i ++)
                            {
                                company.ticker = [getObjectsWithinArray objectAtIndex:i];
                                //company.ticker = arrayofData[i];
                                company.name = strings;
    
                                if (self.companiesForTableView.count < 1)
                                {
                                    [self.companiesForTableView addObject:company];
                                    //NSLog(@"count is %lu",(unsigned long)self.companiesForTableView.count);
                                }
                                else {
                                    NSString *checkTicker = company.ticker;
                                    NSString *checkName = company.name;
    
                                    BOOL absent = NO;
                                    for (Company *companies in self.companiesForTableView)
                                    {
    
                                        if ((checkTicker != companies.ticker) && (checkName != companies.name))
                                        {
                                            absent = YES;
    
                                        }
                                        else {
                                            //do nothing
                                            absent = NO;
                                        }
                                        
                                    }
                                    if (absent == YES) {
                                        [self.companiesForTableView addObject:company];
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                self.searchTVC.results = [[NSArray alloc]init];
                self.searchTVC.results = self.companiesForTableView;

                [self.searchTVC.tableView reloadData];
            });
            
        }
        });
}

- (void)queryResults:(NSArray *)results
{

}

#pragma mark Search Table Delegate

- (void)dismissKeyboard
{
    [self.searchBar resignFirstResponder];
}


#pragma mark Buttons
- (IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)filterButton:(id)sender {
}
@end
