//
//  BaseViewController.h
//  projectSnowball
//
//  Created by Donald Fung on 1/15/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompanyManager.h"
#import "UIFont+customFonts.h" 
#import "UIColor+customColors.h"
#import "Constants.h"

//  Screens size
#define screen_width [UIScreen mainScreen].bounds.size.width
#define screen_height [UIScreen mainScreen].bounds.size.height

@protocol BaseViewDelegate <NSObject>
@optional
- (void)transitionToViewController;
- (void)addOrRemoveCompanyFromWatchlist;
@end

@interface BaseViewController : UIViewController <BaseViewDelegate>
{
CGFloat screenWidth;
CGFloat screenHeight;
}
@property (nonatomic, weak) id <BaseViewDelegate> baseViewDelegate;
@property (nonatomic, strong) NSNumberFormatter *currencyFormat;
@property (nonatomic, strong) NSNumberFormatter *decimalFormat;
@property (strong, nonatomic) CompanyManager *companyManager;


// Get company data
- (Company *)getCompanyDataForViewController;

// Top Nav Bar
- (void)setNavBarTitle:(NSString *)title;
- (void)setUpTopNav;
- (void)customizeLeftBarButton;

// Get screen size
- (CGRect)getScreenSize;

// Add tableview as subview
- (UITableViewController *)tableViewAsSubview:(UITableViewController *)tvc withIdentifier:(NSString *)identifier andTableViewHolder:(UIView *)tableviewHolder andFrame:(CGRect)tableviewFrame;

// Summary Table UI
- (UILabel *)customizeLabelForSummaryView:(UILabel *)label;

// Number Formatters
- (NSString *)formatNumberCurrencyStyle:(NSNumber *)number;
- (NSString *)formatNumberNoStyle:(NSNumber *)number;

//  Date format
- (NSString *)getYear:(NSString *)date;

//  Labels
- (UIFont *)setFontToHelveticaWithSizeFourteen;
- (UIFont *)setFontToHelveticaWithSizeSixteen;
@end
