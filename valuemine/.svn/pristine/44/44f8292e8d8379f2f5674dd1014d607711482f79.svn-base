//
//  CompanyRatiosManager.m
//  projectSnowball
//
//  Created by Donald Fung on 1/14/16.
//  Copyright © 2016 Donald Fung & Blayne Cameron. All rights reserved.
//

#import "CompanyRatiosManager.h"
#import "ParseClient.h"
#import "Company.h"
#import "Constants.h"

@interface CompanyRatiosManager ()

@end
@implementation CompanyRatiosManager

+ (id)sharedRatiosManager
{
    static CompanyRatiosManager *sharedRatiosManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedRatiosManager = [[self alloc] init];
    });
    return sharedRatiosManager;
}

- (NSMutableDictionary *)ratios:(NSDictionary *)QuandlData
{
    NSNumber *earningsPerShare;
    NSString *fiscalYearEndDate;
    NSArray *epsStream = [QuandlData valueForKey:@"EPS"];
    if (epsStream.count == 0) {
        earningsPerShare = [NSNumber numberWithDouble:0];
    }
    else {
        NSArray *lastEpsItem = [epsStream objectAtIndex:0];
        
        //  Earnings Per Share
        earningsPerShare = [lastEpsItem objectAtIndex:1];
        //  Fiscal year end date
        fiscalYearEndDate = [lastEpsItem objectAtIndex:0];
    }
    
    
    
    
    //  Historical Prices
    NSArray *priceStream = [QuandlData valueForKey:@"PRICE"];
    
    NSNumber *fiscalYearEndPrice;
    for (int i = 0; i < priceStream.count; i ++) {
        NSMutableArray *priceData = [priceStream objectAtIndex:i];
        NSString *date = [priceData objectAtIndex:0];
        if ([fiscalYearEndDate isEqualToString: date]) {
            //  Fiscal year end price
           fiscalYearEndPrice = [priceData objectAtIndex:1];
        }
    }
    NSNumber *totalAsset;
    NSArray *totalAssetsStream = [QuandlData valueForKey:@"ASSETS"];
    if (totalAssetsStream.count == 0) {
        totalAsset = [NSNumber numberWithDouble:0];
    }
    else {
        NSArray *latestTotalAssetsData = [totalAssetsStream objectAtIndex:0];
        //  Total Assets
        totalAsset = [latestTotalAssetsData objectAtIndex:1];
    }
    
    NSNumber *totalLiabilities;
    NSArray *totalLiabilitiesStream = [QuandlData valueForKey:@"LIABILITIES"];
    if (totalLiabilitiesStream.count == 0) {
        totalLiabilities = [NSNumber numberWithDouble:0];
    }
    else {
        NSArray *latestTotalLiabilitiesData = [totalLiabilitiesStream objectAtIndex:0];
        //  Total Liabilities
       totalLiabilities = [latestTotalLiabilitiesData objectAtIndex:1];
    }
    
    NSNumber *intangibleAsset;
    NSArray *intangiblesStream = [QuandlData valueForKey:@"INTANGIBLES"];
    if (intangiblesStream.count == 0) {
        intangibleAsset = [NSNumber numberWithDouble:0];
    }
    else {
        NSArray *latestIntangiblesData = [intangiblesStream objectAtIndex:0];
        //  Intangible Assets
      intangibleAsset = [latestIntangiblesData objectAtIndex:1];
    }
    
    
    //  Shares Outstanding
    NSNumber *numSharesOutstanding;
    NSArray *sharesStream = [QuandlData valueForKey:@"SHARESWA"];
    if (sharesStream.count == 0) {
        numSharesOutstanding = [NSNumber numberWithDouble:0];
    }
    else {
        NSArray *sharesData = [sharesStream objectAtIndex:0];
        numSharesOutstanding = [sharesData objectAtIndex:1];
    }
    
    
    //  Total Debt
    NSNumber *debt;
    NSArray *debtStream = [QuandlData valueForKey:@"DEBT"];
    if (debtStream.count == 0) {
        debt = [NSNumber numberWithDouble:0];
    }
    else{
        NSArray *latestDebtData = [debtStream objectAtIndex:0];
        debt = [latestDebtData objectAtIndex:1];
    }
    
    //  Shareholders Equity
    NSNumber *equity;
    NSArray *shareholdersEquityStream = [QuandlData valueForKey:@"EQUITY"];
    if (shareholdersEquityStream.count == 0) {
        equity = [NSNumber numberWithDouble:0];
    }
    else{
        NSArray *latestShareholdersEquityData = [shareholdersEquityStream objectAtIndex:0];
        equity = [latestShareholdersEquityData objectAtIndex:1];
    }
   

    //  Inventory
    NSNumber *inventoryItem;
    NSArray *inventoryStream = [QuandlData valueForKey:@"INVENTORY"];
    if (inventoryStream.count == 0) {
        inventoryItem = [NSNumber numberWithDouble:0];
    }
    else {
        NSArray *lastInventoryItem = [inventoryStream objectAtIndex:0];
       inventoryItem = [lastInventoryItem objectAtIndex:1];
    }
    
    
    //  PE Ratio
    NSNumber *priceToEarningsRatio = [NSNumber numberWithDouble:([fiscalYearEndPrice doubleValue] / [earningsPerShare doubleValue])];
    
    //  Book Value
    NSNumber *bookVal = [NSNumber numberWithDouble:([totalAsset doubleValue] - ([intangibleAsset doubleValue] + [totalLiabilities doubleValue]))];
    
    //  Book Value Per Share
    NSNumber *book_ValuePerShare = [NSNumber numberWithDouble:([bookVal doubleValue] / [numSharesOutstanding doubleValue])];
    
    //  Price to Book Value
    NSNumber *pbValue = [NSNumber numberWithDouble:([fiscalYearEndPrice doubleValue] / [book_ValuePerShare doubleValue])];
    
    //  Debt to Capital Ratio
    NSNumber *debtToCapRatio = [NSNumber numberWithDouble:([debt doubleValue] / ([debt doubleValue] + [equity doubleValue]))];
    
    //  Current Assets
    NSNumber *currentAsset;
    NSArray *totalCurrentAssetsStream = [QuandlData valueForKey:@"ASSETSC"];
    if (totalCurrentAssetsStream.count == 0) {
        currentAsset = [NSNumber numberWithDouble:0];
    }
    else{
        
        NSArray *latestCurrentAssetsData = [totalCurrentAssetsStream objectAtIndex:0];
        currentAsset = [latestCurrentAssetsData objectAtIndex:1];
    }
    
    //  Current Liabilities
    NSNumber *currentLiability;
    NSArray *currentLiabilitiesStream = [QuandlData valueForKey:@"LIABILITIESC"];
    if (currentLiabilitiesStream.count == 0) {
        currentLiability = [NSNumber numberWithDouble:0];
    }
    else{
        NSArray *lastCurrentLiabilityItem = [currentLiabilitiesStream objectAtIndex:0];
        currentLiability = [lastCurrentLiabilityItem objectAtIndex:1];
    }
    NSNumber *current_Ratio;
    NSNumber *qckRatio;
    NSNumber *ncav;
    //  Current Ratio & Quick Ratio
    if (currentAsset == [NSNumber numberWithDouble:0] && currentLiability == [NSNumber numberWithDouble:0]) {
        // Zeroed if Nan or infinity exists
        current_Ratio = [NSNumber numberWithDouble:0];
        qckRatio = [NSNumber numberWithDouble:0];
        ncav = [NSNumber numberWithDouble:0];
    }
    else {
        //  Current Ratio
        current_Ratio = [NSNumber numberWithDouble:([currentAsset doubleValue] / [currentLiability doubleValue])];
        //  Quick Ratio
        qckRatio = [NSNumber numberWithDouble:(([currentAsset doubleValue] - [inventoryItem doubleValue]) / [currentLiability doubleValue])];
        //  NCAV
        ncav = [NSNumber  numberWithDouble:([currentAsset doubleValue] - [totalLiabilities doubleValue])];
    }
    
    //  Dividends Per Share
    NSArray *dividendsPerShareStream = [QuandlData valueForKey:@"DPS"];
    NSArray *dpsData = [dividendsPerShareStream objectAtIndex:0];
    NSNumber *dpsNumer = [dpsData objectAtIndex:1];
    
#warning calculate the correct value
    NSNumber *peg = [NSNumber numberWithDouble:([priceToEarningsRatio doubleValue])];
    
    //  Dictionary to contain ratios for each new company
    NSMutableDictionary *companyRatios = [[NSMutableDictionary alloc]init];
    [companyRatios setValue:priceToEarningsRatio forKey:pe_ratio];
    [companyRatios setValue:bookVal forKey:book_value];
    [companyRatios setValue:book_ValuePerShare forKey:book_value_per_share];
    [companyRatios setValue:pbValue forKey:price_to_book_value];
    [companyRatios setValue:debtToCapRatio forKey:debt_to_capital_ratio];
    [companyRatios setValue:current_Ratio forKey:current_ratio];
    [companyRatios setValue:qckRatio forKey:quick_ratio];
    [companyRatios setValue:dpsNumer forKey:dividends_per_share];
    [companyRatios setValue:ncav forKey:net_current_asset_value];
    [companyRatios setValue:peg forKey:price_earnings_growth];
    return companyRatios;
}


@end
